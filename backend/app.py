from flask import Flask, request
from flask_cors import CORS
import psycopg
import smtplib
import ssl
import email
import config

app = Flask(__name__)
CORS(app)

# verify database connection & create table
# try:
#     with psycopg.connect(config.PGSQL_CONNECTION_STRING) as conn:
#         with conn.cursor() as cur:
#             cur.execute("CREATE TABLE IF NOT EXISTS messages (name TEXT, email TEXT, message TEXT NOT NULL);")
#             conn.commit()
# except:
#     print("Fatal Error: Could not connect to " + config.PGSQL_CONNECTION_STRING)
#     exit(1)

# connect to SMTP server
def smtpConnect():
    try:
        sslCtx = ssl.SSLContext(ssl.PROTOCOL_TLS)
        smtp = smtplib.SMTP(config.SMTP_SERVER, config.SMTP_PORT)
        smtp.ehlo()
        smtp.starttls(context=sslCtx)
        smtp.ehlo()
        smtp.login(config.SMTP_USER, config.SMTP_PASS)

        return smtp
    except:
        print("Error: Could not connect to " + config.SMTP_SERVER)
        print("Email will not be sent!")

    return None

# send notification email
def sendEmail(name, userEmail, message):
    emailMsg = email.message.EmailMessage()
    emailMsg.set_default_type('text/plain')

    emailMsg['From'] = 'donotreply@vlfldr.com'
    emailMsg['To']   = [ 'steve@vlfldr.com' ]
    emailMsg['Subject'] = 'vlfldr.com :: New contact form submission'

    emailMsg.set_content(f'''\
A new contact form submission was received from https://vlfldr.com/contact!

Name: {name}
Email: {userEmail}
Message: {message}

This is an automated message and cannot be replied to.''')

    try:
        smtp = smtpConnect()
        smtp.sendmail('donotreply@vlfldr.com', [ 'steve@vlfldr.com' ], emailMsg.as_string())
        smtp.quit()
    except Exception as e:
        print(e)
        smtp.quit()
        return False

    return True

# insert into postgreSQL 'messages' table
# def insertMessage(name, userEmail, message):
#     try:
#         with psycopg.connect(config.PGSQL_CONNECTION_STRING) as conn:
#             with conn.cursor() as cur:
#                 # psycopg automatically sanitizes input to prevent SQLi
#                 cur.execute(
#                     "INSERT INTO messages (name, email, message) VALUES (%s, %s, %s)",
#                     (name, userEmail, message))
#                 conn.commit()
#     except:
#         return False
#     return True

@app.route("/submit-form", methods=['POST'])
def submit_form():
    if request.form['message'].strip() == '':
        return 'Empty Submission'

    nm = request.form['name']
    em = request.form['email']
    msg = request.form['message']

    if not sendEmail(nm, em, msg):
        return 'Failure'

    # if not insertMessage(nm, em, msg):
    #     return 'Failure'

    return 'Success'

if __name__ == '__main__':
    app.run('0.0.0.0', 9102, debug=True)
