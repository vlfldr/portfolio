import os

SMTP_USER = os.getenv('SMTP_USER')
SMTP_PASS = os.getenv('SMTP_PASS')

SMTP_SERVER = os.getenv('SMTP_SERVER')
SMTP_PORT = os.getenv('SMTP_PORT')

#PGSQL_CONNECTION_STRING = os.getenv('PGSQL_CONNECTION_STRING')
