import yaml
import sass
import os
import shutil
import time
import threading
from jinja2 import Environment, FileSystemLoader, select_autoescape
from wkhtmltopdf import wkhtmltopdf
from http.server import HTTPServer, SimpleHTTPRequestHandler

# input:    YAML standard resume, SASS styles
# output:   Identical Vue component, Jinja2 template, PDF file
#
# depends on package `wkhtmltopdf` (available via apt and dnf)
# run in dev environment or during build - paths won't work in Docker

WORK_DIR = "./resume"
RESUME_FILE = "standardResume.yml"
SASS_PATH = "../../../frontend/sass"
FONTS_PATH = "../../../../frontend/public/fonts"

VUE_TEMPLATE_FILE = "ResumePageTemplate.vue"
VUE_OUTPUT_FILE = "../frontend/src/views/ResumePage.vue"
PDF_TEMPLATE_FILE = "PDF_Template.html"
PDF_OUTPUT_FILE = "../Resume.pdf"

TEMP_WEBSERVER_PORT = 9109
RESUME_NUM_CS_COURSES = 3   # number of comp. sci (vs. game programming) courses

def writeHTML(file, template):
    file.write(template.render(
        rd=resumeData, 
        numCompSciCourses=RESUME_NUM_CS_COURSES
    ))

def copyFontsRecursive(path, subDir=''):
    for f in os.listdir(path):
        target = os.path.join(path, f)
        if os.path.isdir(target):
            os.mkdir(f)
            copyFontsRecursive(target, '/' +f)
        else:
            shutil.copy(target, os.getcwd() + subDir)

# jinja setup
env = Environment( loader=FileSystemLoader(WORK_DIR), autoescape=None )
vueTemplate, PDFTemplate = env.get_template(VUE_TEMPLATE_FILE), env.get_template(PDF_TEMPLATE_FILE)

# load file
srcFile = os.path.join(WORK_DIR, RESUME_FILE)
with open(srcFile, 'r') as f:
    resumeData = yaml.safe_load(f)
    print("Found input file " + os.path.join(WORK_DIR, RESUME_FILE))

# render resume + template into vue component
with open(VUE_OUTPUT_FILE, 'w') as fOut:
    writeHTML(fOut, vueTemplate)
    print('-> Wrote Vue component to ' + VUE_OUTPUT_FILE)

# create temp directory
tempPath = os.path.join(WORK_DIR, 'temp')
print("Creating temp directory: " + tempPath)
try:
    os.mkdir(tempPath)
except FileExistsError:
    shutil.rmtree(tempPath)
    os.mkdir(tempPath)
os.chdir(tempPath)

# write temp HTML
# TODO: new template
with open('index.html', 'w') as fOut:
    writeHTML(fOut, PDFTemplate)
    print('-> Wrote HTML to ' + os.path.join(tempPath, 'index.html'))

# compile SASS from frontend
sass.compile(dirname=(SASS_PATH, os.getcwd()))

# copy fonts from frontend
os.mkdir('fonts')
os.chdir('fonts')

copyFontsRecursive(FONTS_PATH)

os.chdir('..')

# serve temp folder
httpd = HTTPServer(("localhost", TEMP_WEBSERVER_PORT), SimpleHTTPRequestHandler)
thread = threading.Thread(target=httpd.serve_forever)
thread.daemon = True
thread.start()
print("Serving " + tempPath + " on localhost:" + str(TEMP_WEBSERVER_PORT))

# render to PDF
wkhtmltopdf(
    url='http://localhost:9109', 
    output_file=PDF_OUTPUT_FILE,
    margin_bottom=0,
    margin_top=0,
    margin_left=0,
    margin_right=0,
    disable_smart_shrinking=True,
    no_background=True
)
print('-> Wrote PDF to ' + os.path.join(os.getcwd(), PDF_OUTPUT_FILE))

# wait to ensure wkHTMLtoPDF has time to visit & print
time.sleep(3)

print("Shutting down server...")
httpd.shutdown()

# wait for server thread to shutdown
time.sleep(3)

# remove temp directory
print("Removing temporary files...")
os.chdir('../..')
if os.path.basename(os.getcwd()) == 'backend':
    shutil.rmtree(tempPath)

print("Success!")