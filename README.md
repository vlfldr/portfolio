<h1 align="center">vlfldr's website 🏡</h1>
<p align="center">This repo contains the source code for <a href="https://vlfldr.com">my personal website</a></p>
<p align="center">
<img src="https://codeberg.org/vlfldr/portfolio/raw/branch/master/screenshot.png">
</p>

### 🐋 Fully Dockerized

### 👌 Reactive SPA

### 📱 Mobile and desktop layouts

### 🛠 Built from scratch

### 😤 95%+ in all Lighthouse categories

Frontend stack: [Vue](https://github.com/vuejs/vue), [Vite](https://github.com/vitejs/vite), [SASS](https://github.com/sass/sass), [Three.js](https://github.com/mrdoob/three.js), GLSL

Backend stack: Python, [Flask](https://github.com/pallets/flask), [PostgreSQL](https://www.postgresql.org/)
