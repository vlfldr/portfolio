// Creates fontawesome subsets to deliver the smallest file possible

const fs = require('fs')
const path = require('path')
const Glob = require("glob")
const YAML = require('yaml')
const subsetFont = require('subset-font')

function defaultTask(cb) {
    const faFile = require.resolve('@fortawesome/fontawesome-free')
    const faMetadata = path.resolve(faFile, '../../metadata/icons.yml')
    const faWebFonts = path.resolve(faFile, '../../webfonts')
    const outDir = path.resolve('./public/fonts')

    const iconYaml = fs.readFileSync(faMetadata, 'utf8')
    const icons = YAML.parse(iconYaml)

    const iconSubset = [
        "arrow-up-right-from-square",   // not sure if this name is correct
        "pen-to-square",
        "arrows-rotate",
        "question",
        "magnifying-glass",
        "xmark",
        "gamepad",
        "caret-right",
        "circle-right",
        "user",
        "globe",
        "envelope",
        "code-branch",
        //"github-square",
        //"linkedin",
        "paper-plane",
        "location-pin",
        "upload",
        "download"
    ]

    let iconSubSetUnicode = []
    let i = 0;

    iconSubset.forEach((ico) => {
        //console.log(icons[ico])

        console.log('.fa-' + iconSubset[i] + '::before {\n\tcontent: \"\\' + icons[ico].unicode + '\"; }')
        i+=1;
        iconSubSetUnicode.push(String.fromCodePoint(parseInt(icons[ico]['unicode'], 16)))
    })

    const outputTypes = [
        { targetFormat: 'woff2', fileExt: 'woff2' }    ]

    

    Glob.sync(faWebFonts + '/*.ttf').forEach((entry) => {
        //console.log(entry)

        
        const fontData = fs.readFileSync(entry);

        outputTypes.forEach((ftype) => {

            const subsetBuffer = subsetFont(fontData, iconSubSetUnicode.join(' '), {
                targetFormat: ftype.targetFormat,
            });

            subsetBuffer.then((data) => {
                const fName = path.parse(entry).name + '.' + ftype.fileExt;
                const fPath = path.join(outDir, fName)
                fs.writeFile(fPath, data, err => {
                    if (err) {
                        console.error(err);
                    }
                });
            })

        })
    })

    cb();
}

exports.default = defaultTask