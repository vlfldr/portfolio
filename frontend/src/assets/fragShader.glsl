#include <common>

#define MARCH_STEPS 64
#define MARCH_DIST 100.0
#define MARCH_TERMINATE 0.1

#define LIGHT_X 0
#define LIGHT_Y 3
#define LIGHT_Z 0

uniform vec3 iResolution;
uniform float iTime;
uniform float iRadius;
uniform vec2 iMouse;
uniform int iFrame;

// references:
// https://iquilezles.org/www/index.htm
// https://iquilezles.org/www/material/nvscene2008/rwwtt.pdf
// https://www.cs.drexel.edu/~david/Classes/Papers/rtqjs.pdf
// https://mercury.sexy/hg_sdf/

// return distance from point p to sphere
// sphere = (x, y, z, radius)
float dSphere(vec3 p, vec4 sphere){
    return length(p - sphere.xyz) - sphere.w;
}

// infinite plane facing towards planeNorm,
// at a specified distance away from 0,0,0
float dPlane(vec3 p, vec3 planeNorm, float distFromOrigin){
    return dot(p, planeNorm) - distFromOrigin;
}

// torus = (x, y, z, radius)
// rThick = thickness/radius of outer pipe
float dTorus(vec3 p, vec4 torus, float rThick){
    // point on floor (y=0) beneath p
    vec3 pFloor = vec3(p.x, 0, p.z);
    // distance from floor (y=0) to large ring
    float dFloor = length(pFloor.xz - torus.xz) - torus.w;
    
    // instead of calculating angle and using law of cosines,
    // use additive properties of vectors to get
    // distance from p to center of outer pipe
    float dRing = length(vec2(dFloor, p.y));
    // subtract radius of outer pipe to get final distance
    return dRing - rThick;
}

// rounded capsule with endpoints [cap0, cap1] and specified radius
float dCapsule(vec3 p, vec3 cap0, vec3 cap1, float radius){
    // vector from ray search point to center of cap0
    vec3 toCap0 = p - cap0;
    // line segment between capsule endpoints
    vec3 axis = cap1 - cap0;
    
    // project line to cap0 onto axis; map length to axis length
    // this gives us a value that represents the relationship
    // between p's angle/magnitude and the defined capsule
    float axisOffset = dot(toCap0, axis) / dot(axis, axis);
    // clamp to [0, 1] to represent [cap0, cap1]
    axisOffset = clamp(axisOffset, 0., 1.);
    
    // advance along axis with above value to get
    // the point along axis which ray is looking squarely at
    vec3 capsuleHitPoint = cap0 + (axis * axisOffset);
    
    // define capsule based on p's distance to above point
    return length(p - capsuleHitPoint) - radius;
}

void pR(inout vec2 p, float a) {
	p = cos(a)*p + sin(a)*vec2(p.y, -p.x);
}

//////////////////////////////////////////////////

// returns distance from ray search point to SDF
float calcScene(vec3 rayPoint) {

    pR(rayPoint.xz, iMouse.x);
    pR(rayPoint.xy, iMouse.y);
    
    // "light"
    vec4 fakeLight = vec4(LIGHT_X, float(LIGHT_Y)+.5, LIGHT_Z+1, .1);
    fakeLight.x += sin(iTime) * 2.;
    fakeLight.z += cos(iTime) * 2.;
    float dFakeLight = dSphere(rayPoint, fakeLight);
    // remove on transition
    if(iRadius < .4)    dFakeLight = 1000.;
    
    // floor
    float floorY = -1.;
    vec3 floorNorm = vec3(0., 1., 0.);
    float dFloor = dPlane(rayPoint, floorNorm, floorY);
    
    // capsule
    vec3 cap0 = vec3(-.3, 0., 0);
    vec3 cap1 = vec3(.3, .3, 0);
    float dCap = dCapsule(rayPoint, cap0, cap1, iRadius / 3.);

    // torus
    vec4 torus = vec4(0, 1, 0, 2.);     // position & size
    torus.y = sin(iTime);               // move up and down along Y axis
    rayPoint.z *= -5.;                  // mutate
    float dTor = dTorus(rayPoint-torus.xyz, torus, .2);

    return min( min(dFakeLight, min(dCap, dTor)), dFloor);
}

// march along ray intelligently based on SDF
// if returns >= MARCH_DIST, ray did not hit scene
float march(vec3 camPos, vec3 rayNorm) {
    float rayDist = 0.0;
    
    for(int i = 0; i < MARCH_STEPS; i++) {
        // move on ray to previous iteration's end point
        vec3 rayPoint = camPos + (rayNorm * rayDist);
        // get exact distance from ray search point to scene
        float distToGeometry = calcScene(rayPoint);
        // advance along the ray that far, as we will not hit anything
        rayDist += distToGeometry;
        
        // if ray hits geometry, return distance from camera to hit point
        if(rayDist > MARCH_DIST) break;
        // if ray goes past a predefined max distance, there was no hit
        if(distToGeometry <= MARCH_TERMINATE) break;
        // otherwise, we are still searching
    }
    
    return rayDist;
}

// returns point's normal vector by calculating average
// of three points .1 unit away on each axis
vec3 getNormal(vec3 point) {
    float distToGeometry = calcScene(point);
    vec2 diff = vec2(0.1, 0);
    
    return normalize(vec3(distToGeometry - vec3(
        calcScene(point - diff.xyy),
        calcScene(point - diff.yxy),
        calcScene(point - diff.yyx)
    )));
}

// simple shading using dot product of normals
// returns 1.0 if light is parallel to rayHit, 0.0 if perpendicular
float getSimpleShading(vec3 hitPoint) {
    vec3 lightPos = vec3(LIGHT_X, LIGHT_Y, LIGHT_Z);
    
    // rotate camera
    lightPos.x += sin(iTime) * 2.;
    lightPos.z += cos(iTime) * 2.;
    
    vec3 lightNorm = normalize(lightPos - hitPoint);
    vec3 hitPointNorm = getNormal(hitPoint);
    float illumination = 1.;
    
    // march another ray back from the hit point to the light
    // start slightly offset to avoid terminating march prematurely
    float distToLight = march(hitPoint + hitPointNorm * 0.15, lightNorm);
    // if it hits something before it gets to the light,
    if(distToLight < length(lightPos - hitPoint)){
        // we are in a shadow
        illumination = .3;
    }
    
    // a light directly above will illuminate more than angled.
    // dot product is 1 if light norm + point norm are identical,
    // 0 at a right angle, and -1 when parallel -
    // clamp this to [0, 1] to get simple shading value
    return clamp(dot(hitPointNorm, lightNorm), 0., 1.) * illumination;
}

//////////////////////////////////////////////////

void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    
    // Normalized pixel coordinates (from 0 to 1)
    vec2 uv = (fragCoord-.5*iResolution.xy) / iResolution.y;
    
    vec3 camPos = vec3(0, 1, -14);
    vec3 rayNorm = normalize(vec3(uv.x, uv.y, 1));

    float rayDist = march(camPos, rayNorm);
    if(rayDist >= MARCH_DIST) {     // if no hit, return transparent
        fragColor = vec4(0.);
        return;
    }
    vec3 hitPoint = camPos + (rayNorm * rayDist);
    
    vec3 col = vec3(getSimpleShading(hitPoint));
    
    fragColor = vec4(col,1.0);
}
 
void main() {
  mainImage(gl_FragColor, gl_FragCoord.xy);
}