#include <common>


//         .M"""bgd           mm
//        ,MI    "Y           MM
//        `MMb.      .gP"Ya mmMMmm `7MM  `7MM `7MMpdMAo.
//          `YMMNq. ,M'   Yb  MM     MM    MM   MM   `Wb
//        .     `MM 8M""""""  MM     MM    MM   MM    M8
//        Mb     dM YM.    ,  MM     MM    MM   MM   ,AP
//        P"Ybmmd"   `Mbmmd'  `Mbmo  `Mbod"YML. MMbmmd'
//                                              MM
//                                            .JMML.


////// PI CONSTANTS //////
#define M_PI   3.14159265358979323846264338327950288
#define M_PI_4 .785398163397448309615660845819875721
#define M_PI_6 .523598775598298873077107230546583814

////// LIGHT POSITION //////
#define LIGHT_X 0
#define LIGHT_Y 3
#define LIGHT_Z -3

////// RAYMARCH SETTINGS //////
#define MARCH_STEPS 100
#define MARCH_DIST 100.0
#define MARCH_TERMINATE .001

////// UNIFORMS //////
uniform vec3 iResolution;
uniform float iTime;
uniform float iRadius;
uniform int iFrame;
uniform vec2 iMouse;
//uniform float iTimeDelta;


//                              ,,    ,,
//        `7MMF'   `7MF' mm     db  `7MM
//          MM       M   MM           MM
//          MM       M mmMMmm `7MM    MM  ,pP"Ybd
//          MM       M   MM     MM    MM  8I   `"
//          MM       M   MM     MM    MM  `YMMMa.
//          YM.     ,M   MM     MM    MM  L.   I8
//           `bmmmmd"'   `Mbmo.JMML..JMML.M9mmmP'

// PRNG
float rand(float co) { return fract(sin(co*(91.3458)) * 47453.5453); }
float rand2(vec2 co){ return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453); }
float rand3(vec3 co){ return rand(co.xy+rand(co.z)); }

// rotate around axis
void pR(inout vec2 p, float a) {
	p = cos(a)*p + sin(a)*vec2(p.y, -p.x);
}
// smooth step combine
float opSmoothUnion( float d1, float d2, float k ) {
    float h = clamp( 0.5 + 0.5*(d2-d1)/k, 0.0, 1.0 );
    return mix( d2, d1, h ) - k*h*(1.0-h); }


//         .M"""bgd `7MM"""Yb. `7MM"""YMM
//        ,MI    "Y   MM    `Yb. MM    `7
//        `MMb.       MM     `Mb MM   d ,pP"Ybd
//          `YMMNq.   MM      MM MM""MM 8I   `"
//        .     `MM   MM     ,MP MM   Y `YMMMa.
//        Mb     dM   MM    ,dP' MM     L.   I8
//        P"Ybmmd"  .JMMmmmdP' .JMML.   M9mmmP'


////// BASIC SDFS //////
// sphere = (x, y, z, radius)
float dSphere(vec3 p, vec4 sphere){
    return length(p - sphere.xyz) - sphere.w;
}
// torus = (x, y, z, radius)
float dTorus(vec3 p, vec4 torus, float rThick){
    vec3 pFloor = vec3(p.x, p.y, 0);
    float dFloor = length(pFloor.xy - torus.xy) - torus.w;
    float dRing = length(vec2(dFloor, p.z));
    return dRing - rThick;
}
    
///// COMPLEX SDFS /////
float dHexPrism( vec3 p, vec2 h ) {
  const vec3 k = vec3(-0.8660254, 0.5, 0.57735);
  p = abs(p);
  p.xy -= 2.0*min(dot(k.xy, p.xy), 0.0)*k.xy;
  vec2 d = vec2(
       length(p.xy-vec2(clamp(p.x,-k.z*h.x,k.z*h.x), h.x))*sign(p.y-h.x),
       p.z-h.y );
  return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}
// s = size
float dOctahedron( vec3 p, float s) {
  p = abs(p);
  float m = p.x+p.y+p.z-s;
  vec3 q;
       if( 3.0*p.x < m ) q = p.xyz;
  else if( 3.0*p.y < m ) q = p.yzx;
  else if( 3.0*p.z < m ) q = p.zxy;
  else return m*0.57735027;
    
  float k = clamp(0.5*(q.z-q.y+s),0.0,s); 
  return length(vec3(q.x,q.y-s+k,q.z-k)); 
}

float dCylinder( vec3 p, float r, float h ) {
  vec2 d = abs(vec2(length(p.xz),p.y)) - vec2(h,r);
  return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

// TODO: implement angle
float dCylinderArrow(vec3 p, float r, float h, float angle) {
    p.y += h * M_PI;
    p.x -= h * M_PI;
    pR(p.xy, M_PI_4);
    float c1 = dCylinder(p, r, h);
    pR(p.xy, -M_PI_4);
    p.x += h * M_PI * 2.;
    
    pR(p.xy, -M_PI_4);
    float c2 = dCylinder(p, r, h);
    return opSmoothUnion(c1, c2, .1);
}


//         .M"""bgd
//        ,MI    "Y
//        `MMb.      ,p6"bo   .gP"Ya `7MMpMMMb.  .gP"Ya
//          `YMMNq. 6M'  OO  ,M'   Yb  MM    MM ,M'   Yb
//        .     `MM 8M       8M""""""  MM    MM 8M""""""
//        Mb     dM YM.    , YM.    ,  MM    MM YM.    ,
//        P"Ybmmd"   YMbmd'   `Mbmmd'.JMML  JMML.`Mbmmd'


float calcScene(vec3 rayPoint) {
    // mouse controls
    pR(rayPoint.xz, iMouse.x);
    pR(rayPoint.xy, iMouse.y);

    // homepage
    if(iRadius > .5) {
        // rotate world (pR BEFORE mod)
        pR(rayPoint.xy, iTime/20.);
        pR(rayPoint.xz, iTime/20.);

        // domain repetition
        rayPoint.xy = mod(rayPoint.xy, 12.) - 6.;
    }

    // sidebar
    else {
        pR(rayPoint.xz, iTime/10.);
    }

    // rotate individual flake (pR AFTER mod)
    pR(rayPoint.xy, iTime/10.);

    // random
    // int tCenter = int(floor(clamp(rand(iTime) * 3., 0., 3.)));
    // int tSpike  = int(floor(clamp(rand2(rayPoint.xz * iTimeDelta) * 2., 0., 2.)));
    // int tOuter  = int(floor(clamp(rand3(rayPoint.xyz * sin(iTime)) * 2., 0., 2.)));
    
    // showcase
    int f = int(floor(float(iFrame) / 1350.));
    int tCenter = f % 3;
    int tSpike = f % 2;
    int tOuter = f % 2;
    
    float flake = 1000.;
    vec3 rayPointOrig = vec3(rayPoint.x, rayPoint.y, rayPoint.z);
    
    //construct one spike...
    for(int i = 0; i < 6; i++) {
        rayPoint = rayPointOrig;

        // ...mirror 6 times around Z axis
        pR(rayPoint.xy, M_PI_6 * 2. * float(i));
        rayPoint.y -= 1.;
    
        float dCenter = 0.;
        float dSpike = 0.;
        float dOuter = 1000.;
        float yOffset = 0.;

        switch(tCenter) {
            case 0:
                dCenter = dSphere(rayPoint, vec4(0, .2, 0, .6));
                yOffset = 1.5;
                break;
            case 1:
                dCenter = dTorus(rayPoint, vec4(0, .2, 0, .5), .1);
                yOffset = 1.8;
                break;
            case 2:
                dCenter = dOctahedron(rayPoint, .6);
                yOffset = 1.4;
                break;
        }

        rayPoint.y -= yOffset;
        switch(tSpike) {
            case 0:
                dSpike = dCylinder(rayPoint, 1.2, .2);
                break;
            case 1:
                dSpike = dHexPrism(rayPoint.xzy, vec2(.2, 1.2));
                break;
        }

        rayPoint.y -= 1.;
        switch(tOuter) {
            case 0:
                dOuter = dOctahedron(rayPoint, .7);
                break;
            case 1:
                dOuter = dCylinderArrow(rayPoint, 1.1, .2, 2.);
                break;
            
        }

        float combine1 = ((sin(iTime) + 1.) / 2.) + .3;
        float combine2 = ((cos(iTime) + 2.) / 2.) ;

        float spike = opSmoothUnion(opSmoothUnion(dCenter, dSpike, combine1), dOuter, combine2);
        flake = opSmoothUnion(spike, flake, .3);    
    }
    return flake;
}


//                                                  ,,          ,,
//        `7MMM.     ,MMF'                        `7MM          db
//          MMMb    dPMM                            MM
//          M YM   ,M MM   ,6"Yb.  `7Mb,od8 ,p6"bo  MMpMMMb.  `7MM  `7MMpMMMb.  .P"Ybmmm
//          M  Mb  M' MM  8)   MM    MM' "'6M'  OO  MM    MM    MM    MM    MM :MI  I8
//          M  YM.P'  MM   ,pm9MM    MM    8M       MM    MM    MM    MM    MM  WmmmP"
//          M  `YM'   MM  8M   MM    MM    YM.    , MM    MM    MM    MM    MM 8M
//        .JML. `'  .JMML.`Moo9^Yo..JMML.   YMbmd'.JMML  JMML..JMML..JMML  JMML.YMMMMMb
//                                                                             6'     dP
//                                                                             Ybmmmd'


float march(vec3 camPos, vec3 rayNorm) {
    float rayDist = 0.0;
    
    for(int i = 0; i < MARCH_STEPS; i++) {
        vec3 rayPoint = camPos + (rayNorm * rayDist);
        float distToGeometry = calcScene(rayPoint);
        rayDist += distToGeometry;
        
        if(rayDist > MARCH_DIST || distToGeometry <= MARCH_TERMINATE) break;
    }
    return rayDist;
}

vec3 getNormal(vec3 point) {
    float distToGeometry = calcScene(point);
    vec2 diff = vec2(.001, 0);
    
    return normalize(vec3(distToGeometry - vec3(
        calcScene(point - diff.xyy),
        calcScene(point - diff.yxy),
        calcScene(point - diff.yyx)
    )));
}

// simple shading using dot product of normals
float getSimpleShading(vec3 hitPoint) {
    vec3 lightPos = vec3(LIGHT_X, LIGHT_Y, LIGHT_Z);
    
    vec3 lightNorm = normalize(lightPos - hitPoint);
    vec3 hitPointNorm = getNormal(hitPoint);
    float illumination = 1.;
    
    // shadow: march another ray back from the hit point to the light
    float distToLight = march(hitPoint + hitPointNorm * 0.15, lightNorm);
    if(distToLight < length(lightPos - hitPoint)){
        illumination = .3;
    }
    
    return clamp(dot(hitPointNorm, lightNorm), 0., 1.) * illumination;
}

vec3 patternColor(vec3 position, float time) {
    float pattern = sin(position.x * 10.0 + (time * 0.25) * 5.0) * cos(position.y * 10.0 + (time * 0.25) * 5.0);
    pattern = pattern * 0.5 + 0.5; // Normalize to the range [0, 1]
    return vec3(pattern, pattern, pattern);
}

vec3 pulseColor(float time) {
    vec3 color1 = vec3(0.0235, 0.8392, 0.6275); // green accent
    vec3 color2 = vec3(0.9098, 0.8824, 0.9373); // blue accent

    // Use sin function to interpolate between the two colors over time
    float pulse = sin(time) * 0.5 + 0.5;
    return mix(color1, color2, pulse);
}

//                                   ,,
//        `7MMM.     ,MMF'           db
//          MMMb    dPMM
//          M YM   ,M MM   ,6"Yb.  `7MM  `7MMpMMMb.
//          M  Mb  M' MM  8)   MM    MM    MM    MM
//          M  YM.P'  MM   ,pm9MM    MM    MM    MM
//          M  `YM'   MM  8M   MM    MM    MM    MM
//        .JML. `'  .JMML.`Moo9^Yo..JMML..JMML  JMML.


void mainImage( out vec4 fragColor, in vec2 fragCoord ) {
    vec2 uv = (fragCoord-.5*iResolution.xy) / iResolution.y;
    vec3 rayNorm = normalize(vec3(uv.x, uv.y, 1));

    vec3 camPos = vec3(0, 9, -18);
    
    if(iRadius < .5) {      // shrink scene based on uniform
        camPos.z = -35.;   
    }

    float rayDist = march(camPos, rayNorm);

    if(rayDist >= MARCH_DIST) {     // if no hit, return transparent
        fragColor = vec4(.027, .231, .298, 1.);
        return;
    }

    vec3 hitPoint = camPos + (rayNorm * rayDist);
    vec3 col = vec3(getSimpleShading(hitPoint));    

    vec3 patColor = patternColor(col, iTime);
    vec3 pulseCol = pulseColor(iTime);

    // Combine everything; use shading to modulate the contributions of pulse and pattern
    vec3 color2 = col * (0.5 * pulseCol + 0.5 * patColor);

    fragColor = vec4(color2,1.0);
}

////// ENTRYPOINT //////
void main() {
  mainImage(gl_FragColor, gl_FragCoord.xy);
}