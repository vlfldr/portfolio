import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/HomePage.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/resume",
    name: "Resume",
    component: () =>
      import( "../views/ResumePage.vue"),
  },
  {
    path: "/projects",
    name: "Projects",
    component: () =>
      import("../views/ProjectsPage.vue"),
  },
  {
    path: "/contact",
    name: "Contact",
    component: () =>
      import("../views/ContactPage.vue"),
  },
  {
    path: "/projects/raymarching",
    name: "Raymarching",
    component: () =>
      import("../views/projects/RaymarchingProj.vue"),
  },
  {
    path: "/projects/wayland-hyprland",
    name: "Rolling your own Wayland Desktop Environment, Part 1: Hyprland",
    component: () =>
      import("../views/projects/WaylandHyprland.vue"),
  },
  {
    path: "/projects/platformer",
    name: "Platformer",
    component: () =>
      import("../views/projects/PlatformerProj.vue"),
  },
  {
    path: "/projects/ucourse",
    name: "uCourse",
    component: () =>
      import("../views/projects/uCourse.vue"),
  },
  {
    path: "/projects/mycodiaries",
    name: "MycoDiaries",
    component: () =>
      import("../views/projects/MycoDiaries.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ left: 0, top: 0 })
      }, 500)
    })
  }
});


export default router;
