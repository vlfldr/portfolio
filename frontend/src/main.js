import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import axios from 'axios';
import VueAxios from 'vue-axios';
import Notifications from '@kyvg/vue3-notification'

import "@/../sass/main.scss";

const app = createApp(App).use(router);

app.use(VueAxios, axios);
app.use(Notifications);
app.provide('axios', app.config.globalProperties.axios);
app.mount("#app");
