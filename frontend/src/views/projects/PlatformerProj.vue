<template>
  <div class="page__content">

    <button @click="scrollTop()" title="Back to top"
      class="btn-normal to-top-btn">
      <i class="to-top-btn--icon fa-solid fa-circle-right"></i>
    </button>

    <div class="article-wrapper">
    <h1 id="pure-javascript-platformer">Pure JavaScript Platformer</h1>
    <h3 id="gamifying-webpages-in-3kb-of-js">Gamifying Webpages in &lt;3KB&gt; of JS</h3>
    <p>
      The interactive <a href="/resume">resume</a> page of this website is like an ogre. Or an onion, or something. Point is it consists entirely of three
      <strong>canvas</strong> elements stacked on top of each other - there is no text in the DOM. Read on to learn more about why and how it works.
    </p>
    <h2 id="reasoning">Reasoning</h2>
    <p>
      There are plenty of JavaScript graphics libraries out there. It would've been easier - and better for performance - to use one of them (
      <a href="https://github.com/pixijs/pixijs">PixiJS</a>,
      <a href="https://github.com/photonstorm/phaser">Phaser</a>,
      <a href="https://github.com/liabru/matter-js">Matter.js</a>,
      <a href="https://threejs.org/">Three.js</a>, etc.) for this project. However, after becoming familiar with a couple of these libraries, I thought it would be fun to try and do it all from scratch in a relatively simple vanilla JS project. Turns out “relatively simple” can be a relative term! Here's what I learned along the way.
    </p>
    <h2 id="sprites">Sprites</h2>
    <p>The first step of initialization is to load sprite sheets, one for the player and one for the environment. Each sheet consists of dozens of sprites packed into a grid. Here is every 16x16 tile used in the game:</p>
    <figure>
      <img src="/game/img/tilesheet.png" alt />
      <figcaption>A nicely packed spritesheet, courtesy of <a href="https://trixelized.itch.io/starstring-fields">Trixie</a>.</figcaption>
    </figure>
    <p>Because tiles are either 16px squares or rectangular sets of 16px squares, we can describe their positions with grid indices:</p>
    <pre><code>// sprite dimensions
    export const SPRITES = {
        0: { name: &#39;Air&#39;,
            x: 0, y: 1,
            w: 1, h: 1
        },
        1: { name: &#39;Ground&#39;,
            x: 0, y: 9,
            w: 1, h: 1
        },
        2: { name: &#39;VineSmall1&#39;,
            x: 1, y: 2,
            w: 1, h: 1
        },
        ...
        9: { name: &#39;BushBig&#39;,
            x: 4, y: 7,
            w: 5, h: 2
        }
        ...
    }</code></pre>
    <p>
      This allows us to translate between sprite names, their locations within the sprite sheet, and arbitrary
      <strong>tile codes</strong>. Without these codes, the map could be represented like so:
    </p>
    <pre><code>Air Air Air Ground Air Air Ground
    Air Air Air VineSmall1 Air Air Ground
    Ground Ground Ground Ground Air Ground</code></pre>
    <p>
      Unpleasantly aligned yet usable. One big issue is that
      <strong>storing our map in this format would take up a prohibitive amount of space</strong>. To uncover the less obvious issue, let's represent the above snippet with tile codes:
    </p>
    <pre><code>00 00 00 01 00 00 01
    00 00 00 02 00 00 01
    01 01 01 01 00 01</code></pre>
    <p>
      The issue is immediately apparent - we're short a tile! It's also much easier to understand what's above/below each tile. For these three reasons, the
      <strong>tilemap</strong> is constructed from
      <strong>tile codes</strong>.
    </p>
    <h2 id="tilemap">Tilemap</h2>
    <p>
      The tilemap starts out as a block of plain text. Rows of tiles are separated by newlines; individual tiles are separated by spaces. Each number corresponds to a different sprite - for example
      <code>01</code> is a solid block and
      <code>13</code> is the small plant to the right of the player.
    </p>
    <figure>
      <img src="/images/platformer_0.png" alt />
      <figcaption>A rendered area's corresponding tile codes. The player's location is highlighted.</figcaption>
    </figure>
    <p>
      Each code corresponds to a sprite &amp; set of properties. For example, the
      <code>13</code> next to the highlighted region corresponds to a decorative plant to the right of the player. This information is used to build a 2D array of Tile objects representing the game world. Note that while the entire tilemap is loaded at once, only a subset of tiles (those within the viewport) are being drawn each frame.
    </p>
    <p>
      Those with sharp eyes may notice a strange value to the left of the player -
      <code>0E</code>. This is one of three special tile codes:
    </p>
    <ul>
      <li>
        <code>0E</code> - Part of another
        <strong>non-solid</strong> sprite
      </li>
      <li>
        <code>0F</code> - Part of another
        <strong>solid</strong> sprite
      </li>
      <li>
        <code>L?</code> - Where
        <code>?</code> is any character - render text of specified character
      </li>
    </ul>
    <p>
      Sprites larger than 1x1 tiles are represented with a single instance of their tile code. The remaining tiles are filled with
      <code>0E</code> or
      <code>0F</code>, depending on whether the oversize sprite is solid or not.
    </p>
    <figure>
      <img src="/images/platformer_1.png" alt />
      <figcaption>
        Player standing on solid
        <code>0F</code> tiles
      </figcaption>
    </figure>
    <p>
      For example, the 6x1 ground piece above has a tile code of
      <code>36</code> and is then filled with
      <code>0F</code> tiles.
      <strong>
        <code>0F</code> fill tiles extend solid objects and will trigger collisions.
      </strong>
    </p>
    <p>
      On the other hand, the 2x2 decorative rock (
      <code>07</code>) is not solid, so it was filled with
      <code>0E</code> tiles. This serves as a reminder that a sprite occupies the tile.
      <strong>
        <code>0E</code> fill tiles act like air tiles.
      </strong>
    </p>
    <p>
      Any code beginning with the letter L - such as
      <code>LH</code>,
      <code>Li</code>,
      <code>L!</code>, etc - will be rendered as text tiles. These tiles are always solid with the exception of
      <code>L</code> (space). Making this process data-driven means we can create interactive text without querying the position of DOM elements each frame. This improves performance &amp; produces more accurate collisions.
    </p>
    <p>
      <img src="/images/platformer_text.png" alt="An example of text rendering" /> ## Camera &amp; Viewport The tilemap can be arbitrarily large, but the viewport is always a fixed size (52 x 32 tiles). The camera is simply a 2D vector describing
    </p>
    <h2 id="architecture-requestanimationframe">Architecture &amp; requestAnimationFrame()</h2>
    <h2 id="physics">Physics</h2>
    <h3 id="jump-termination">Jump Termination</h3>
    <p>
      Jump termination is a seemingly small tweak that has a huge impact on gameplay. The concept is as follows: holding the jump button should produce a higher, longer jump than tapping it. There's a great write-up
      <a href="https://2dengine.com/?p=platformers">here</a> a
    </p>
    <figure>
      <video src="/videos/variable_jumps.mp4" controls>
        <a href="/videos/variable_jumps.mp4"></a>
      </video>
      <figcaption>Variable jump height</figcaption>
    </figure>
    <h3 id="wall-grabbing-jumping">Wall Grabbing &amp; Jumping</h3>
    <p>
      Wall jumping is a tried and true gameplay mechanic. If the player collides on the x axis
      <strong>and</strong> is in the air, they “stick” to the wall (stop on both axes) and the
      <code>onWall</code> variable is set:
    </p>
    <figure>
      <video src="/videos/wall_grab.mp4" controls>
        <a href="/videos/wall_grab.mp4"></a>
      </video>
      <figcaption>Wall grabbing &amp; stamina</figcaption>
    </figure>
    <pre><code>if( xCollision() ) {
        ...
        velocity.x = 0;
        
        // wall grab
        if( isJumping ) {
            onWall = Math.sign( velocity.x );
            velocity.y = 0;
        }
        ...
        
    } else    onWall = 0;</code></pre>
    <p>
      The main idea is to store the direction the player hit the wall from by storing their velocity at the moment of collision in
      <code>onWall</code>. When the grab ends,
      <code>xCollision()</code> will return false and
      <code>onWall</code> is set to 0. But if the player jumps while
      <code>onWall</code> equals either -1 or 1, they will be propelled in the opposite direction:
    </p>
    <figure>
      <video src="/videos/wall_jump.mp4" controls>
        <a href="/videos/wall_jump.mp4"></a>
      </video>
      <figcaption>Wall jumping</figcaption>
    </figure>
    <pre><code>if( jumpKey &amp;&amp; canJump ) {
        velocity.y = INITIAL_JUMP_VEL;
        
        // wall jump
        if( onWall !== 0 ) {
            velocity.x = -2 * onWall;
            onWall = 0;
        }
        ...
    }</code></pre>
    <p>
      This makes wall jumps feel more satisfying and realistic. In the same vein, it wouldn't feel satisfying to let go of the arrow key one frame too early and not be able to jump because
      <code>onWall</code> has been reset. To solve this - and make jumping off ledges feel better, too - we'll implement something called
      <strong>Coyote Time</strong>.
    </p>
    <h2 id="coyote-time">Coyote Time</h2>
    <p>
      Named after Mr. Wile E. himself, coyote time is a brief window in which the player can still jump
      <strong>after leaving the ground</strong>. In the real world - or a 2D physics system attempting to approximate the real world - this doesn't make any sense. But in terms of
      <em>game feel</em>, that brief window makes enough of a difference to have lingo dedicated to it.
    </p>
    <figure>
      <img src="/images/platformer_2.png" alt />
      <figcaption>In a well-tuned platformer, Wile E. could jump here!</figcaption>
    </figure>
    <p>
      Coyote time is simple to understand and implement. Keep track of the last time the player could've actually jumped, and if
      <code>COYOTE_TIME</code> has not elapsed since then, fudge the rules and continue to allow them to jump. Here's how it feels to press the jump key two frames after leaving a ledge:
    </p>
    <figure>
      <video src="/videos/no_coyote_time.mp4" controls>
        <a href="/videos/no_coyote_time.mp4"></a>
      </video>
      <figcaption>Wall grabbing &amp; stamina</figcaption>
    </figure>
    <p>This may be realistic, but it feels bad to play. Here's the same input with coyote time enabled:</p>
    <figure>
      <video src="/videos/coyote_time.mp4" controls>
        <a href="/videos/coyote_time.mp4"></a>
      </video>
      <figcaption>Coyote time jump - key frames slowed down</figcaption>
    </figure>
    <pre><code>if( jumpKey &amp;&amp; ( canJump || framesSinceGrounded &lt; COYOTE_FRAMES ) ) {
        velocity.y = INITIAL_JUMP_VEL;
        framesSinceGrounded += COYOTE_FRAMES;
        
        // wall jump
        if( onWall !== 0 ) {
            velocity.x = -2 * onWall;
            onWall = 0;
        }
    }
    ...

    if( canJump ) {
        framesSinceGrounded = 0;
    } else {
        framesSinceGrounded++;
    }</code></pre>
    <p>
      Here you'll notice the new
      <code>framesSinceGrounded</code> variable, which is incremented while in the air and reset when the player touches ground. We set it to a value larger than COYOTE_FRAMES to avoid triggering multiple mid-air jumps.
      <strong>
        Increasing the
        <code>COYOTE_FRAMES</code> constant will make running off platforms feel much more forgiving by increasing the window in which the player can jump.
      </strong>
    </p>
    <p>
      This also means that wall jump inputs no longer need to be frame-perfect, which feels better to play. But it presents a problem: we're only using one variable (
      <code>onWall</code>) to store both the wall grab state
      <em>and</em> direction of approach. Once
      <code>onWall</code> equals 0, there's no way to tell which way the coyote frame jump should push the player, so it's possible to jump straight up for a few frames after leaving the wall.
    </p>
    <p>
      We could solve this in two ways: splitting
      <code>onWall</code> into a boolean and velocity component or store the previous
      <code>onWall</code> state until coyote frames expire. I opted for the second approach:
    </p>
    <pre><code>if ( xCollision() ) {
        ...
        velocity.x = 0;
        
        // wall grab
        if( isJumping ) {
            onWall = Math.sign( velocity.x );
            velocity.y = 0;
        }
        ...
        
    // no X collision &amp; previously on wall
    } else if ( onWall !== 0 ) {
        prevOnWall = onWall;
        onWall = 0;
        
        framesOffWall = 1;
    }

    // window in which prevOnWall will be used
    if (framesOffWall &gt; 0) {
        framesOffWall++;
        
        if( framesOffWall &gt; COYOTE_FRAMES ) {
            prevOnWall = 0;
            framesOffWall = 0;
        }
    }</code></pre>
    <p>
      There's a lot going on here. The first block is unchanged - we're still sticking to the wall and storing the direction of impact in
      <code>onWall</code>. The second block - which runs a frame after letting go of the wall - resets
      <code>onWall</code> (so the grab ends) but stores its value in
      <code>prevOnWall</code>. This is the velocity that will be applied to a wall jump input in the coyote frame window. It then starts the
      <code>framesOffWall</code> timer, which the final block simply increments and resets. We finish up by modifying the jump function:
    </p>
    <pre><code>if( jumpKey &amp;&amp; 
        (canJump
        || framesSinceGrounded &lt; COYOTE_FRAMES
        || ( framesOffWall &gt; 0 &amp;&amp; framesOffWall &lt; COYOTE_FRAMES ) ) 
    ) {
        velocity.y = INITIAL_JUMP_VEL;
        framesSinceGrounded += COYOTE_FRAMES;
        
        // wall jump
        if( onWall !== 0 ) {
            velocity.x = -2 * onWall;
            onWall = 0;
        }
        
        if(prevOnWall !== 0) {
            velocity.x = -2 * prevOnWall;
            prevOnWall = 0;
            framesOffWall = 0;
        }
    }</code></pre>
    <p>
      Same as before with two modifications: - Allow jumping if player was previously on the wall within coyote time window - Use the velocity stored in
      <code>prevOnWall</code> to execute said jump
    </p>
    <p>
      This allows for better back-and-forth wall jumping and fixes the issue mentioned earlier where it was possible to jump straight up the wall in the coyote time window after letting go.
      <strong>Fudging the laws of physics like this is dangerous</strong> because it can introduce unexpected edge cases and spghetti code, but in small doses it makes the game
      <em>feel</em> much better to play. End result:
    </p>
    <figure>
      <video src="/videos/ct_wall_jump.mp4" controls>
        <a href="/videos/ct_wall_jump.mp4"></a>
      </video>
      <figcaption>Coyote time wall jump - key frames slowed down</figcaption>
    </figure>
    <h2 id="pixel-perfect-canvas-scaling">Pixel Perfect Canvas Scaling</h2>
    <h2 id="font-rendering">Font Rendering</h2>
    <h2 id="tilemap-editor">Tilemap Editor</h2>
    <h2 id="mobile-controls">Mobile Controls</h2>
  </div>
  </div>
</template>

<script setup>

function scrollTop() {
  document.documentElement.scrollTop = 0
}
</script>
