<template>
  <div class="page__content">
    <button
      @click="scrollTop()"
      title="Back to top"
      class="btn-normal to-top-btn"
    >
      <i class="to-top-btn--icon fa-solid fa-circle-right"></i>
    </button>
    <Teleport to="body">
      <ZoomedImage
        :show="showModal"
        :imgPath="modalImg"
        @close="showModal = false"
      />
    </Teleport>

    <div class="article-wrapper">
      <h1>uCourse</h1>
      <h3>Featuring Bun, Hono, SolidJS, and UnoCSS</h3>

      <p>
        <a href="https://codeberg.org/vlfldr/ucourse">uCourse</a> is a
        self-hosted video streaming platform for educational content. Just like
        <a href="https://www.navidrome.org/">Navidrome</a> is a personal Spotify
        or <a href="https://jellyfin.org/">Jellyfin</a> is a personal Netflix,
        uCourse is a personal Udemy. Point it at your library, import your
        courses, and watch from any browser on the LAN. As a developer, small
        projects are good excuses to check out new frameworks and tools that
        might be too immature for a larger project.
      </p>
      <figure>
        <img
          loading="lazy"
          @click="zoomImage('/ucourse/player_screenshot.png')"
          src="/ucourse/player_screenshot.png"
          alt="uCourse video player"
        />
        <figcaption aria-hidden="true">uCourse video player</figcaption>
      </figure>
      <h2>Motivation</h2>
      <p>
        Users of self-hosted software have saved collective millions in
        subscription fees. Navidrome, Jellyfin, or
        <a href="https://calibre-ebook.com/">Calibre</a> have you covered when
        it comes to <s>pirating</s> legally archiving music, movies, or books.
        But what about educational videos? There seems to be plenty available to
        download and no library management software. uCourse is intended to fill
        this gap.
      </p>
      <em
        >Disclaimer: course creators deserve to be paid for their work. If you
        pirate a course and get a lot out of it, consider purchasing afterwards
        or making a donation to the instructor.</em
      >
      <h2>Choosing the stack</h2>
      <p>
        One of the main goals of this project was to get acquainted with up and
        coming tools I had never used before. This way, even if uCourse doesn’t
        drum up much interest, the development process itself is a valuable
        learning exercise.
      </p>

      <figure>
        <table class="tech-table">
          <colgroup>
            <col style="width: 20%" />
            <col style="width: 20%" />
            <col style="width: 60%" />
          </colgroup>
          <thead>
            <tr class="header">
              <th>Tech used</th>
              <th>Instead of</th>
              <th>Because</th>
            </tr>
          </thead>
          <tbody>
            <tr class="odd">
              <td>Bun</td>
              <td>Node.js</td>
              <td>
                The new runtime on the block. Bun boasts massive performance
                improvements and backwards compatibility with Node APIs.
              </td>
            </tr>
            <tr class="even">
              <td>SolidJS</td>
              <td>Vue/React</td>
              <td>
                Solid’s granular approach to reactivity is refreshingly simple.
                It also consistently beats all other frameworks in speed and
                resource usage benchmarks.
              </td>
            </tr>
            <tr class="odd">
              <td>Hono</td>
              <td>Express/FastAPI</td>
              <td>
                Hono is a bleeding-edge API framework with native Bun support.
                It’s 16 times faster than Express on average.
              </td>
            </tr>
            <tr class="even">
              <td>UnoCSS</td>
              <td>Tailwind</td>
              <td>
                UnoCSS positions itself as a fully atomic engine, whereas
                Tailwind is more of an opinionated framework. Uno’s Tailwind
                preset allows us to use familiar Tailwind syntax while enjoying
                Uno’s superior developer experience, pure SVG icons, and smaller
                bundle size.
              </td>
            </tr>
            <tr class="odd">
              <td>Drizzle ORM</td>
              <td>Prisma</td>
              <td>
                Drizzle is “thinner” than Prisma in that there’s less layers of
                abstraction between your code and the SQL. Probably the most
                mature project in this list.
              </td>
            </tr>
          </tbody>
        </table>
        <figcaption id="table-caption" aria-hidden="true">
          Comparison of tooling across the stack
        </figcaption>
      </figure>
      <h2>Look and feel</h2>
      <p>
        User interfaces should be intuitive and unobtrusive. Although I don’t
        agree with every aspect of the
        <a href="https://material-minimal.com/">Material Minimal</a> philosophy,
        it maintains that <strong>content is the focus</strong> of any UI. This
        rings true - the path to delighting users lies in focusing on and
        attractively presenting the content they care about, not in adding more
        animations and gradients. Inspiration for uCourse’s interface is drawn
        from this school of thought as well as projects which embody it:
      </p>
      <div id="image-container">
        <figure>
          <img
            loading="lazy"
            @click="
              zoomImage(
                'https://github.com/wg-easy/wg-easy/raw/master/assets/screenshot.png'
              )
            "
            src="https://github.com/wg-easy/wg-easy/raw/master/assets/screenshot.png"
            alt="wg-easy"
          />
          <figcaption aria-hidden="true">wg-easy user interface</figcaption>
        </figure>
      </div>
      <p>
        <a href="https://emilenijssen.nl/">Emile Nijssen</a> ‘s
        <a href="https://github.com/wg-easy/wg-easy">wg-easy</a> is a great
        example of clean content-focused design. It stands out among hundreds of
        self-hosted web UIs for its:
      </p>
      <ul>
        <li>Stark simplicity</li>
        <li>Attention to detail</li>
        <li>
          Colors and animations used sparingly to great effect
          <ul style="margin-bottom: -10px">
            <li>
              The eye is naturally drawn to red, used to highlight the most
              important elements
            </li>
            <li>
              Card edges animate according to connected clients’ up/down speeds
            </li>
          </ul>
        </li>
      </ul>

      <p>
        There is creativity, personality, and modernity here - this school of
        thought is not “no fun allowed retvrn to 1998”. It’s just that
        everything is carefully considered, nothing extraneous is added, and
        <em>each part of the UI serves an explicit purpose</em>. uCourse aims to
        emulate this spartan content-focused design without sacrificing features
        or polish.
      </p>
      <figure>
        <img
          loading="lazy"
          @click="zoomImage('/ucourse/course_list_screenshot.png')"
          src="/ucourse/course_list_screenshot.png"
          alt="uCourse user interface"
        />
        <figcaption aria-hidden="true">uCourse user interface</figcaption>
      </figure>
      <h2>Architecture</h2>
      <p>
        The backend is a Hono API which uses <code>bun:sqlite</code> to
        interface with an SQLite database. Users curate a course media folder on
        their machine. These files are then accessed via the frontend - a
        SolidJS SPA - to import, edit, delete, and watch courses. Progress is
        automatically tracked and saved to the database.
      </p>
      <h2>Takeaways: the good...</h2>
      <p>
        The biggest win is that uCourse was realized as it was originally
        imagined: a small, performant, self-hosted video streaming tool.
        Everything loads lightning fast, bundle size is measured in double
        digits, strenuous operations like importing and streaming put barely any
        load on the server. A perfect lighthouse score never hurts either.
      </p>
      <p>
        I didn’t do anything particularly clever or groundbreaking to achieve
        this. Bun, SQLite, Hono, and Solid are all fast as heck. Maintaining
        this need for speed throughout the stack - ensuring there was no
        bottleneck to hold any layer back - was the key to unlocking incredible
        performance.
      </p>

      <h2>The bad...</h2>
      <p>
        The stack wasn&apos;t all performance improvements and rainbows. A
        significant amount of time was spent getting frameworks to play nice
        with each other. The first hurdle was incorporating UnoCSS. There are a
        handful of UI component libraries built specifically for Uno, but they
        don&apos;t have nearly as much to offer as equivalent Tailwind libraries
        like
        <a title="https://daisyui.com/" href="https://daisyui.com/">DaisyUI</a>.
        There&apos;s a community-maintained
        <a
          title="https://github.com/kidonng/unocss-preset-daisy"
          href="https://github.com/kidonng/unocss-preset-daisy"
          >preset</a
        >
        for UnoCSS, but documentation is sparse. Massaging config files to get
        Uno and Daisy working together without Tailwind took longer than it had
        any right to.
      </p>
      <figure>
        <img
          width="400"
          loading="lazy"
          @click="zoomImage('/ucourse/limes_guy.jpg')"
          src="/ucourse/limes_guy.jpg"
          alt="why cant i, hold all these tools?"
        />
        <figcaption aria-hidden="true">
          why cant I, hold all these tools?
        </figcaption>
      </figure>
      <p>
        Even after solving this issue, Bun refused to build the project with
        DaisyUI included. I was able to patch the offending lines out of
        DaisyUI, but it resulted in modals being positioned incorrectly, and the
        error only occured when building with the Bun runtime (<code
          class="inline-code"
          >bunx --bun vite build</code
        >). The fix was simple - switching to
        <code class="inline-code">bunx vite build</code> to build with Node -
        but I&apos;m still not sure why generating DaisyUI modal styles breaks
        Bun.
      </p>
      <p>
        The biggest downside was a quantitative and qualitative lack of
        documentation. SolidJS has the developer&apos;s best interests at heart,
        but information on any specific concept is fragmented across too many
        sections (tutorial, guides, API reference) with philosophy and practical
        examples comingled. One never knows when clicking on the first search
        result for &apos;solidJS resources&apos; if they&apos;re going to end up
        back at the interactive tutorial, or at a couple paragraphs explaining
        the concept of resources, or at the API reference they&apos;re looking
        for. The information is all there, it&apos;s just not laid out as well
        as it could be.
      </p>
      <p>
        The Hono and UnoCSS documentation sites were light on information,
        didn&apos;t fully cover their respective frameworks, and each featured
        circular navigation loops (Getting Started -&gt; Integrations -&gt;
        Examples -&gt; Integrations, etc). This doesn&apos;t make the tools any
        less powerful, but certainly raises the barrier of entry and frustration
        level when integrating into a full project.
      </p>
      <p>
        Bun, DaisyUI, and Drizzle were general exceptions to this rule. Their
        documentation was a pleasure to navigate. Anyone who has written
        documentation knows that organizating all that constantly changing
        information isn&apos;t easy. Big props to all three projects&apos;
        technical writers.
      </p>

      <h2>And the JS runtime bug</h2>
      <p>
        The nastiest issue in this project by a long shot came at the end of
        development while testing on other browsers. I use Firefox where videos
        always worked as expected - but they wouldn’t play in <em>any</em> other
        browser! It would be less than ideal to release video streaming software
        which was incapable of streaming videos.
      </p>
      <p>
        After spending days testing on different machines, swapping video player
        and API frameworks out, digging into NGINX configurations, tweaking
        experimental Chromium flags, and re-encoding videos, hope and sanity
        were waning. That’s when I saw that the video streaming endpoint
        responded with two <em>Content-Range</em> headers - including one with
        no total file size which wasn’t being created in the code. Firefox
        happily discards the duplicate header and plays the video while WebKit
        (Chrome, Chromium, Edge, Safari, Brave) chokes. But where was it coming
        from?
      </p>
      <figure>
        <img
          loading="lazy"
          @click="zoomImage('/ucourse/bun_bug.png')"
          src="/ucourse/bun_bug.png"
          alt="bun content-range header bug"
        />
        <figcaption aria-hidden="true">
          The moment of realization: "Firefox is OK with those two headers and
          plays the video well, but Chrome does not."
        </figcaption>
      </figure>
      <p>
        Then someone filed
        <a href="https://github.com/oven-sh/bun/issues/7051">a bug</a>
        describing a duplicated Content-Range header which breaks video playback
        in Chrome but not Firefox! This bug wasn't in the frontend framework,
        the backend framework, the video player library, video formats, the
        code, or any other sensible place... It was in Bun, the JavaScript
        runtime. It was fixed by GitHub user WingLim with
        <a href="https://github.com/oven-sh/bun/pull/7199">this pull request</a>
        which was merged into
        <a href="https://bun.sh/blog/bun-v1.0.15">Bun v1.0.15</a>. Thanks
        WingLim!
      </p>
      <p>
        In retrospect, it should’ve been obvious that such an immature runtime
        was prone to these types of bugs. Coming from Node, a sensible knee-jerk
        response to “could the site-breaking bug be in the JS runtime instead of
        my code?” is “lmao no, check your code again”. Not the right attitude
        when working with brand new tools! Lesson learned.
      </p>
      <h2>Conclusion</h2>
      <p>
        There’s tremendous educational value in picking a well-scoped project
        and seeing it through to completion, even if the idea seems “too basic”
        or no one ever uses it. Getting one’s hand’s dirty with new tools is
        great for rapidly evaluating their maturity and viability for other
        projects. The obvious drawback to “evaluation/development” is also the
        reason why we get excited about these things: they’re new! This
        introduces a different world of pitfalls and gotchas; critical bugs in
        lower parts of the stack become more common.
      </p>
      <p>
        Thanks for sticking around. Feel free to download or star
        <a href="https://codeberg.org/vlfldr/ucourse">uCourse</a> if you’re
        interested!
      </p>
      <HireMeFooter />
    </div>
  </div>
</template>
  
  <script setup>
import HireMeFooter from "../../components/HireMeFooter.vue";
import ZoomedImage from "../../components/ZoomedImage.vue";
import { ref } from "vue";

const showModal = ref(false);
const modalImg = ref("");

function scrollTop() {
  document.documentElement.scrollTop = 0;
}

function zoomImage(imgPath) {
  showModal.value = true;
  modalImg.value = imgPath;
}
</script>
  