let spriteSheet = new Image();
spriteSheet.src = 'game/img/tilesheet.png';
let paletteCtx;
import * as Consts from '@/resume-game/constants.js';
import Tile from '@/resume-game/tile.js';
import * as Utils from '@/resume-game/utils.js';

export default class Editor {
    constructor(tileMap) {
        this.brushSprite = 1;
        this.textMode = false;
        this.textCell = {x: 0, y: 0};
        this.mousePos = {x: 0, y: 0};
        this.editorEnabled = false;
        this.camY = 0;

        this.tileMap = tileMap.tiles;
        this.paletteCanvas = Utils.createHiPPICanvas('palette-canvas', 
            Consts.SIDEBAR_SIZE / 2, window.innerHeight / 2, 1);
        this.paletteCtx = this.paletteCanvas.getContext('2d');
    }

    mouseDownRef    = this.editorClick.bind(this);
    mouseMoveRef    = this.editorMove.bind(this);
    keyUpRef        = this.handleKeyup.bind(this);

    // add/remove listeners
    toggleEditor(){
        this.gameCanvas = document.getElementById('game-canvas');

        if(this.editorEnabled) {
            this.gameCanvas.removeEventListener("mousedown",    this.mouseDownRef, false);
            this.gameCanvas.removeEventListener("mousemove",    this.mouseMoveRef, false);
            this.gameCanvas.removeEventListener("keyup",        this.keyUpRef,     false);
        } else {
            this.gameCanvas.addEventListener("mousedown",       this.mouseDownRef,  false);
            this.gameCanvas.addEventListener("mousemove",       this.mouseMoveRef,  false);
            this.gameCanvas.addEventListener("keyup",           this.keyUpRef,      false);

            this.textMode = false;
        }

        this.clearPalette();

        this.editorEnabled = !this.editorEnabled;
    }

    clearPalette() {
        this.paletteCtx.clearRect(0, 0, this.paletteCtx.canvas.width, this.paletteCtx.canvas.height);
    }

    draw(ctx, camera) {
        this.drawPalette();
        this.drawCursor(ctx);

        this.camY = camera.y;
    }

    // draw semi-transparent selected sprite at mouse pos
    drawCursor(ctx) {
        let sprite = Consts.SPRITES[this.brushSprite];
        ctx.globalAlpha = .6;
        ctx.drawImage(spriteSheet, sprite.x * Consts.SS_TILE_SIZE, sprite.y * Consts.SS_TILE_SIZE, 
            Consts.SS_TILE_SIZE * sprite.w, Consts.SS_TILE_SIZE * sprite.h, 
            this.roundToTile(this.mousePos.x), this.roundToTile(this.mousePos.y),
            Consts.TILE_SIZE * sprite.w, Consts.TILE_SIZE * sprite.h)
        ctx.globalAlpha = 1;
    }

    drawPalette() {
        let drawX = 0;
        let drawY = 0;
        
        this.clearPalette();
        Object.values(Consts.SPRITES).forEach(s => {
            if(s.h > 6) drawY += 6;
            if(s.name == 'TextBrush')   drawY += 99;
            this.paletteCtx.drawImage(spriteSheet, s.x * Consts.SS_TILE_SIZE, s.y * Consts.SS_TILE_SIZE, 
                Consts.SS_TILE_SIZE * s.w, Consts.SS_TILE_SIZE * s.h, drawX * Consts.TILE_SIZE, drawY * Consts.TILE_SIZE, 
                Consts.TILE_SIZE * s.w, Consts.TILE_SIZE * s.h
            );
            s.palY = drawY;
            drawY += s.h;
            if(drawY >= this.paletteCtx.canvas.height / (Consts.TILE_SIZE)) {
                drawY = 0;
                drawX += 2;
            }
        });
        this.paletteCtx.fillText('A', Consts.SS_TILE_SIZE * 6, Consts.SS_TILE_SIZE)
    }

    // paletteMove(event) { }

    paletteClick(event) { 
        let palX = Math.floor( ((event.layerX) / Consts.TILE_SIZE));
        let palY = Math.floor( (event.layerY / Consts.TILE_SIZE));

        // match X index
        Object.keys(Consts.SPRITES).forEach(k => {
            if(Consts.SPRITES[k].palY == palY) this.brushSprite = k;
        });

        if(palX > 4 && palY < 3)    this.brushSprite = 99;
    }

    // paletteMove(event) { }

    // update mouse pos; fire click&drag events
    editorMove(event) {

        const scaleX = (102 * Consts.TILE_SIZE) / this.gameCanvas.getBoundingClientRect().width;
        const scaleY = (63 * Consts.TILE_SIZE) / this.gameCanvas.getBoundingClientRect().height;

        this.mousePos.x = (event.clientX - Consts.SIDEBAR_SIZE) * scaleX;
        this.mousePos.y = event.clientY * scaleY;
        this.mousePos.y += (this.camY * 2);

        if(event.buttons == 1)    this.editorClick(event);
    }

    editorClick(event) {
        this.textMode = false;

        let mapX = this.roundToTile(this.mousePos.x) / Consts.TILE_SIZE;
        let mapY = this.roundToTile(this.mousePos.y) / Consts.TILE_SIZE;

        // prevent accidental clicks under menu
        if(mapX < 9 && mapY < 3)  return;
        
        // if(mapY > mapHeight) mapY = mapHeight;
        // if(mapX > mapWidth) mapX = mapWidth;

        // engage textMode
        if(this.brushSprite == 99) {
            this.textCell.x = mapX;
            this.textCell.y = mapY;
            this.textMode = true;
        }
        
        let sprite = Consts.SPRITES[this.brushSprite];

        // fill with no-draw for larger than 1x1 sprites
        // 0E = non-solid blocks; 0F = solid blocks
        let fillChar = '0E'
        if(this.brushSprite >= 24 && this.brushSprite <= 36)  fillChar = '0F'
        for(let i = 0; i < sprite.w; i++){
            for(let j = 0; j < sprite.h; j++)   this.tileMap[mapY+j][mapX+i] = new Tile(fillChar);
        } 

        this.tileMap[mapY][mapX] = new Tile(this.brushSprite.toString());
    }

    handleKeyup(event){
        // ignore keypresses outside of text mode
        if(!this.textMode || !this.editorEnabled)   return;
    
        // ignore modifier keypresses
        if(event.key == 'Alt' 
            || event.key == 'Shift' 
            || event.key == 'Control' 
            || event.key == 'Enter'
        )       return;
    
        // delete previously typed chars on backspace
        if(event.key == 'Backspace' 
            && this.textCell.x-1 >= 0 
            && this.tileMap[this.textCell.y][this.textCell.x-1].tileLetter != ''
        ) {
            this.tileMap[this.textCell.y][this.textCell.x-1] = new Tile('00');
            this.textCell.x--;
            return;
        }
    
        this.tileMap[this.textCell.y][this.textCell.x] = new Tile('L' + event.key);
        this.textCell.x++;

        // if(this.textMode && this.textCell.x+1 > mapWidth) this.textMode = false;
    }

    roundToTile(num) { return Math.floor(num/Consts.TILE_SIZE) * Consts.TILE_SIZE; }
}

