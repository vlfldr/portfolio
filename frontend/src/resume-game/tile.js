import { SPRITES } from '@/resume-game/constants.js';

export default class Tile {
    constructor(tileCode) {
        this.tileCode = tileCode;
        if(tileCode[0] == 'L') {
            this.tileLetter = tileCode[1];
            this.tileSolid = true;
            if(tileCode[1] == ' ')  this.tileSolid = false;
            return;
        }

        let tileCodeInt = parseInt(tileCode);
        this.tileSprite = SPRITES[tileCodeInt];

        this.tileDebug = false;

        this.tileSolid = false;
        this.tileCollectible = false;
        this.tileLetter = '';
        this.tileRotation = 0;

        // solid platforms & solid no-draw
        if((tileCodeInt >= 24 && tileCodeInt <= 36) || 
            tileCodeInt == 1 || tileCode == '0F' ) {    
            this.tileSolid = true;
        }

        // one-way platforms
        if(tileCodeInt >= 14 && tileCodeInt <= 18) {
            this.tileOneWay = true;
            this.tileSolid = true;
        }
    }
}