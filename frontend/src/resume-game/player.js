import * as Consts from "./constants.js";

let playerSpritesheet = new Image();
let playerSpritesheetFlipped = new Image();
playerSpritesheet.src = 'game/img/player.png';
playerSpritesheetFlipped.src = 'game/img/player_flipped.png';

export default class Player {
    constructor() {
        this.startPosition = {
            x: Consts.TILE_SIZE * 4,
            y: Consts.TILE_SIZE * 3
        };
        this.position = structuredClone(this.startPosition);
        this.velocity = {
            x: 0.0,
            y: Infinity
        };
        this.size = {
            x: 12,
            y: 20,
        };
        this.moveSpeed = .23;
        this.framesSinceGrounded = 0;
        this.framesOnWall = 0;
        this.wantsWallGrab = false;
        this.animState = 'idle';
        this.animFrame = 0;
        this.flipX = false;
        this.onWall = 0;    // 0 for false, 1 for right, -1 for left
        this.prevOnWall = 0;
    }

    draw(ctx) {
        let drawSheet = (this.flipX) ? playerSpritesheetFlipped : playerSpritesheet;
        let drawFrame = this.animFrame * Consts.SS_PLAYER_W;
        if(this.flipX)    drawFrame = playerSpritesheet.naturalWidth - drawFrame - Consts.SS_PLAYER_W;
    
        ctx.drawImage(drawSheet, drawFrame, Consts.ANIMATIONS[this.animState]['row'] * Consts.SS_PLAYER_H, 
            Consts.SS_PLAYER_W, Consts.SS_PLAYER_H,
            Math.floor(this.position.x) - Consts.SS_X_OFFSET, 
            Math.floor(this.position.y) - Consts.SS_Y_OFFSET,
            Consts.SS_PLAYER_W, Consts.SS_PLAYER_H );
    
        //ctx.strokeRect(this.position.x+.5, this.position.y+.5, this.size.x-1, this.size.y-1);    // debug hitbox
    }

    /* ref: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
        0. Decompose X/Y movement; step X first
    *   1. Get coordinate of leading edge based on velocity
    *   2. Get tiles on OPPOSITE axis intersected by leading edge
    *       IMPORTANT:  Because our max speed is below Consts.TILE_SIZE, we do not have to search more than one tile.
    *           If player can move more than Consts.TILE_SIZE in one frame, additional tiles must be checked.
    *   3. Check collisions with leading edge
    *   4. Snap to tile borders if next position is invalid
    *   5. Advance along X axis; recalc leading edge position
    *   6. Step Y using same process
    */


    //                                           ,,                        
    //            `7MMF'   `7MF'               `7MM           mm           
    //              MM       M                   MM           MM           
    //              MM       M `7MMpdMAo.   ,M""bMM   ,6"Yb.mmMMmm .gP"Ya  
    //              MM       M   MM   `Wb ,AP    MM  8)   MM  MM  ,M'   Yb 
    //              MM       M   MM    M8 8MI    MM   ,pm9MM  MM  8M"""""" 
    //              YM.     ,M   MM   ,AP `Mb    MM  8M   MM  MM  YM.    , 
    //               `bmmmmd"'   MMbmmd'   `Wbmd"MML.`Moo9^Yo.`Mbmo`Mbmmd' 
    //                           MM                                        
    //                         .JMML.                                      

    update(tileMap) {
        // max speed
        if(Math.abs(this.velocity.x) > Consts.MAX_X_SPEED)   
        this.velocity.x = Consts.MAX_X_SPEED * Math.sign(this.velocity.x);

        // damping
        if(this.velocity.x != 0)  this.velocity.x = this.velocity.x / (1 + Consts.DAMPING)

        let nextX = this.position.x + this.velocity.x;
        let xEdge = nextX;
        if(this.velocity.x > 0)   xEdge += this.size.x;

        // horizontal page bounds
        if(xEdge < 0) {
            nextX = 0;
            this.velocity.x = 0;
        }
        if(xEdge > (Consts.TILE_SIZE * (tileMap[0].length-1))){
            nextX = (Consts.TILE_SIZE * (tileMap[0].length-1)) - this.size.x
            this.velocity.x = 0;
        }

        // horizontal object collision
        if(this.xCollision(xEdge, this.position.y, tileMap)) {
            if(this.velocity.x > 0)   nextX = (( this.worldToIndex(xEdge)) * Consts.TILE_SIZE) - this.size.x;
            else                      nextX = (( this.worldToIndex(xEdge) + 1) * Consts.TILE_SIZE);

            // begin wall grab
            if(this.wantsWallGrab && this.velocity.y != Infinity) {
                // prevent unintentional just-walked-off/just-jumped wall grabs
                if(this.onWall == 0 && this.framesSinceGrounded < 15) {
                    this.onWall = 0;

                // onWall stores directional information for wall jump
                } else {
                    this.onWall = Math.sign(this.velocity.x);
                    this.velocity.y = 0;
                }
            }
            this.velocity.x = 0;
        }
        // if no xCollision, but previously on wall...
        else if(this.onWall !== 0) {
            // store old walljump info for coyote frames
            this.prevOnWall = this.onWall;
            this.framesOnWall = -1;
            this.onWall = 0;
        }

        // negative framesOnWall === coyote frames where prevOnWall velocity is used
        if(this.prevOnWall && this.framesOnWall < 0) {
            this.framesOnWall--;

            if(this.framesOnWall < -Consts.COYOTE_TIME_FRAMES) {
                this.prevOnWall = 0;
                this.framesOnWall = 0;
            }
        }

        this.position.x = nextX;

        // on solid ground
        if(this.velocity.y == Infinity) {
            let nextY = this.position.y + Consts.GRAVITY;
            this.framesSinceGrounded = 0;
            
            if(!this.yCollision(this.position.x, nextY, tileMap))  this.velocity.y = 0;
        }

        // falling/jumping
        if(this.velocity.y != Infinity) {
            this.framesSinceGrounded++;

            // wall grabbing: increment frames
            if(this.onWall != 0) {
                this.framesOnWall++;
                this.framesSinceGrounded = 0;

                // slide down wall after 50 frames
                if(this.framesOnWall > 50) {
                    this.velocity.y += Consts.GRAVITY * (this.framesOnWall * 0.02)
                }
            }
            // not wall grabbing: add normal gravity
            else {
                this.velocity.y += Consts.GRAVITY;
            }

            // terminal velocity
            if(this.velocity.y > Consts.MAX_Y_SPEED)     this.velocity.y = Consts.MAX_Y_SPEED;

            let nextY = this.position.y + this.velocity.y;
            let yEdge = nextY;
            if(this.velocity.y > 0)   yEdge += this.size.y;

            // vertical page bounds
            if(yEdge < 0) {
                nextY = 0;
                this.velocity.y = 0;
            }
            if(yEdge > (Consts.TILE_SIZE * (tileMap.length-1))) {
                nextY = (Consts.TILE_SIZE * (tileMap.length-1)) + this.size.y;
                this.velocity.y = 0;
            }

            // vertical object collision
            if(this.yCollision(this.position.x, yEdge, tileMap)) {
                // if hit ceiling, snap to tile below & bump out 1 pixel
                if(this.velocity.y <= 0 ) {
                    nextY = ((this.worldToIndex(yEdge) + 1) * Consts.TILE_SIZE) + 1
                    this.velocity.y = 0;
                }
                // if hit floor, snap above & bump up
                else {
                    nextY = ((this.worldToIndex(yEdge) - 1) * Consts.TILE_SIZE) - 3;   
                    this.velocity.y = Infinity;
                }
            }
            this.position.y = nextY;
        }   
    }

                                                                                        
    //                                        ,,                                          
    //                  db                    db                             mm           
    //                 ;MM:                                                  MM           
    //                ,V^MM.    `7MMpMMMb.  `7MM  `7MMpMMMb.pMMMb.   ,6"Yb.mmMMmm .gP"Ya  
    //               ,M  `MM      MM    MM    MM    MM    MM    MM  8)   MM  MM  ,M'   Yb 
    //               AbmmmqMA     MM    MM    MM    MM    MM    MM   ,pm9MM  MM  8M"""""" 
    //              A'     VML    MM    MM    MM    MM    MM    MM  8M   MM  MM  YM.    , 
    //            .AMA.   .AMMA..JMML  JMML..JMML..JMML  JMML  JMML.`Moo9^Yo.`Mbmo`Mbmmd' 
                                                                                     

    calcAnimationState() {
        let aState = 'idle';

        if(Math.abs(this.velocity.x) > 1)                           aState = 'run'
        if(this.velocity.y != Infinity && this.velocity.y >= 0)     aState = 'fall';
        if(this.velocity.y < 0)                                     aState = 'jump';
        if(this.onWall != 0)                                        aState = 'grab';

        // if(this.animState.includes('to-'))     return this.animState;

        // if(aState == 'grab' && this.animState != 'grab') {
        //     aState = 'to-grab';
        //     this.animFrame = 0;
        // }

        // if(aState == 'jump' && this.animState != 'jump') {
        //     aState = 'to-jump';
        //     this.animFrame = 0;
        // }

        // if(aState == 'jump' && this.animState != 'jump') {
        //     aState = 'to-jump';
        //     this.animFrame = 0;
        // }

        return aState;
    }

    animate() {

        this.animState = this.calcAnimationState();
        
        // cycle frames
        if(this.animFrame >= Consts.ANIMATIONS[this.animState]['frames']){
            if(this.animState.includes('to-'))  this.animState = this.animState.substring(3);
            this.animFrame = 0;
        }
    }
    
                                                                                      
    //                                 ,,    ,,    ,,            ,,                     
    //              .g8"""bgd        `7MM  `7MM    db            db                     
    //            .dP'     `M          MM    MM                                         
    //            dM'       ` ,pW"Wq.  MM    MM  `7MM  ,pP"Ybd `7MM  ,pW"Wq.`7MMpMMMb.  
    //            MM         6W'   `Wb MM    MM    MM  8I   `"   MM 6W'   `Wb MM    MM  
    //            MM.        8M     M8 MM    MM    MM  `YMMMa.   MM 8M     M8 MM    MM  
    //            `Mb.     ,'YA.   ,A9 MM    MM    MM  L.   I8   MM YA.   ,A9 MM    MM  
    //              `"bmmmd'  `Ybmd9'.JMML..JMML..JMML.M9mmmP' .JMML.`Ybmd9'.JMML  JMML.
                                                                    

    // returns TRUE if COLLISION
    xCollision(x, y, tileMap) {
        let yTopTile = this.worldToTile(x, y, tileMap);
        // always search bottom tile as player is 2 tiles high
        let yBotTile = this.worldToTile(x, y + this.size.y - 2, tileMap) 

        // one-way platforms are never an obstacle on X axis
        return (yTopTile && yTopTile.tileSolid && !yTopTile.tileOneWay)
            || (yBotTile && yBotTile.tileSolid && !yBotTile.tileOneWay)
    }

    // returns TRUE if COLLISION
    yCollision(x, y, tileMap) {
        let xLeftTile = this.worldToTile(x, y, tileMap)
        // xRightTile != xLeftTile ONLY IF player is straddling two tiles
        //      otherwise, it will be the same as xLeftTile
        let xRightTile = this.worldToTile(x + this.size.x - 1, y, tileMap)
        
        let hit = (xRightTile && xRightTile.tileSolid)
            || (xLeftTile && xLeftTile.tileSolid)

        // one-way platforms
        // ref: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
        let hitY = this.worldToIndex(y) * Consts.TILE_SIZE;
        if (hit && ((xRightTile && xRightTile.tileOneWay)
            || (xLeftTile && xLeftTile.tileOneWay))
        )       hit = (hitY - this.position.y > Consts.TILE_SIZE);

        return hit
    }

    // returns tile - or null if OOB of tilemap
    worldToTile(x, y, tileMap) { 
        let xTile = this.worldToIndex(x)
        let yTile = this.worldToIndex(y)
        if(yTile > tileMap.length || yTile < 0
            || xTile > tileMap[0].length || xTile < 0)  return null;

        return tileMap[yTile][xTile] 
    }

    // returns 1D tile index
    worldToIndex(x){ return Math.floor( x / Consts.TILE_SIZE );  }
}