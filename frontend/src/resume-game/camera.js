import * as Consts from '@/resume-game/constants.js';

const halfViewportW = Consts.TILE_SIZE * (Consts.VIEWPORT_TILES_W / 2);
const halfViewportH = Consts.TILE_SIZE * (Consts.VIEWPORT_TILES_H / 1.5);

export default class Camera {
    constructor() {
        this.x = 0;
        this.y = 0;


        this.lastX = 0;
        this.lastY = 0;
    }

    setWorldSize(width, height) {
        this.camMaxX = (width) - Consts.VIEWPORT_TILES_W * Consts.TILE_SIZE;
        this.camMaxY = (height) - Consts.VIEWPORT_TILES_H * Consts.TILE_SIZE;
    }


    update(player) {
        this.lastX = this.x;
        this.lastY = this.y;
        
        if(player) {
            this.x = player.position.x - halfViewportW;
            this.y = player.position.y - halfViewportH;
        }

        // canvas bounds
        if (this.x < 0)                 this.x = 0;
        if (this.x > this.camMaxX)      this.x = this.camMaxX;
        if (this.y < 0)                 this.y = 0;
        if (this.y > this.camMaxY)      this.y = this.camMaxY;

        // lerp
        this.x = this.lastX - ( this.lastX - this.x ) * .1;
        this.y = this.lastY - ( this.lastY - this.y ) * .1;

        // use ints for smooth text rendering
        this.x = Math.floor( this.x );
        this.y = Math.floor( this.y );
    }
}