// game logic
export const GRAVITY = .28;
export const DAMPING = .07;
export const JUMP_VEL = -5.2;
export const JUMP_VEL_MIN_TERM = -1.7;
export const MAX_X_SPEED = 14;
export const MAX_Y_SPEED = 14;
export const COYOTE_TIME_FRAMES = 6;

// page layout
export const TILE_SIZE = 16;
export const SIDEBAR_SIZE = 280;
export const VIEWPORT_TILES_W = 52;
export const VIEWPORT_TILES_H = 32;

// tilesheet
export const SS_TILE_SIZE = 16;
export const SS_ROWS = 11;
export const SS_COLS = 18;

// player sprite
export const SS_PLAYER_W = 48;
export const SS_PLAYER_H = 32;
export const SS_X_OFFSET = 18;
export const SS_Y_OFFSET = 13;

// animation states
export const ANIMATIONS = {
    'idle': { frames: 6, row: 0 },
    'run':  { frames: 8, row: 2 },
    'dash': { frames: 5, row: 4 },
    'jump': { frames: 3, row: 6 },
    'fall': { frames: 3, row: 7 },
    'grab': { frames: 1, row: 10 },

    'to-run': { frames: 2, row: 1 },
    'to-stop': { frames: 5, row: 3 },
    'to-jump': { frames: 2, row: 5 },
    'to-grab': { frames: 3, row: 9 },
    'land': { frames: 4, row: 8 },
}

export const ValidRandomTiles = [ 26,27,28,29,30,31,32,33,34,35,36 ]


// tilesheet sprite dimensions
export const SPRITES = {
    99: {
        name: 'TextBrush',
        x: 0, y: 0, w: 1, h: 1
    },
    0: {
        name: 'Air',
        x: 0,
        y: 1,
        w: 1,
        h: 1
    },
    1: {
        name: 'Ground',
        x: 0,
        y: 9,
        w: 1,
        h: 1
    },
    2: {
        name: 'VineSmall1',
        x: 1,
        y: 2,
        w: 1,
        h: 1
    },
    3: {
        name: 'VineSmall2',
        x: 1,
        y: 3,
        w: 1,
        h: 1
    },
    4: {
        name: 'VineMed',
        x: 0,
        y: 2,
        w: 1,
        h: 2
    },
    5: {
        name: 'VineBig',
        x: 2,
        y: 2,
        w: 1,
        h: 3
    },
    6: {
        name: 'RockSmall',
        x: 4,
        y: 5,
        w: 1,
        h: 1
    },
    7: {
        name: 'RockMed',
        x: 3,
        y: 3,
        w: 2,
        h: 2
    },
    8: {
        name: 'BushMed',
        x: 7,
        y: 5,
        w: 3,
        h: 2
    },
    9: {
        name: 'BushBig',
        x: 4,
        y: 7,
        w: 5,
        h: 2
    },
    10: {
        name: 'PlantSmall1',
        x: 9,
        y: 7,
        w: 1,
        h: 1
    },
    11: {
        name: 'PlantSmall2',
        x: 9,
        y: 8,
        w: 1,
        h: 1
    },
    12: {
        name: 'PlantSmall3',
        x: 0,
        y: 4,
        w: 1,
        h: 1
    },
    13: {
        name: 'PlantSmall4',
        x: 1,
        y: 4,
        w: 1,
        h: 1
    },
    14: {
        name: 'PlatformLeftFar',
        x: 5,
        y: 4,
        w: 1,
        h: 1

    },
    15: {
        name: 'PlatformLeft',
        x: 6,
        y: 4,
        w: 1,
        h: 1
    },
    16: {
        name: 'PlatformMid',
        x: 7,
        y: 4,
        w: 1,
        h: 1
    },
    17: {
        name: 'PlatformRight',
        x: 8,
        y: 4,
        w: 1,
        h: 1
    },
    18: {
        name: 'PlatformRightFar',
        x: 9,
        y: 4,
        w: 1,
        h: 1
    },
    19: {
        name: 'GrassSmall1',
         x: 4, y: 6, w: 1, h: 1
    
    },
    20: {
        name: 'GrassSmall2',
         x: 5, y: 6, w: 1, h: 1
    
    },
    21: {
        name: 'GrassSmall3',
         x: 6, y: 6, w: 1, h: 1
    
    },
    22: {
        name: 'GrassCorner1',
         x: 5, y: 5, w: 1, h: 1
    
    }, 
    23: {
        name: 'GrassCorner2',
         x: 6, y: 5, w: 1, h: 1
    
    }, 24: {
        name: 'GroundSmall1',
         x: 0, y: 10, w: 1, h: 1
    
    }, 25: {
        name: 'GroundSmall2',
         x: 1, y: 10, w: 1, h: 1
    
    }, 26: {
        name: 'GroundSmall3',
         x: 4, y: 9, w: 1, h: 1
    
    }, 27: {
        name: 'GroundSmall4',
         x: 5, y: 9, w: 1, h: 1
    
    }, 28: {
        name: 'GroundSmall5',
         x: 17, y: 9, w: 1, h: 1
    
    }, 29: {
        name: 'GroundMed1',
         x: 2, y: 10, w: 2, h: 1
    
    }, 30: {
        name: 'GroundMed2',
         x: 4, y: 10, w: 2, h: 1
    
    }, 31: {
        name: 'GroundMed3',
         x: 6, y: 10, w: 2, h: 1
    
    }, 32: {
        name: 'GroundMed4',
         x: 8, y: 10, w: 2, h: 1
    
    }, 33: {
        name: 'GroundMed5',
         x: 12, y: 9, w: 2, h: 1
    
    }, 34:{
        name: 'GroundBig1',
         x: 1, y: 9, w: 3, h: 1
    
    }, 35:{
        name: 'GroundBig2',
         x: 14, y: 9, w: 3, h: 1
    
    }, 36: {
        name: 'GroundHuge',
         x: 6, y: 9, w: 6, h: 1
    },
    
    37:  {
        name: 'TreeMed',
         x: 0, y: 5, w: 4, h: 4
    }, 
    37:  {
        name: 'TreeBig',
         x: 10, y: 0, w: 8, h: 9
    }
    
}



// keybinds
export const KEY_CODES = {
    68: 'right',
    65: 'left',
    87: 'up',
    32: 'up',
    83: 'down',
    40: 'down',
    38: 'up',
    37: 'left',
    39: 'right'
}
