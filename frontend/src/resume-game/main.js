import * as Consts from '@/resume-game/constants.js';
import * as Utils from '@/resume-game/utils.js';
import Player from '@/resume-game/player.js';
import Camera from '@/resume-game/camera.js';
import InputManager from '@/resume-game/input.js';
import TileMap from '@/resume-game/tilemap.js';
import Editor from '@/resume-game/editor.js';

export let ctx, bgCtx;
export let camera, input, player, tileMap, editor;
 
export let onMobileDevice = /Mobile|Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);
export let gameFrame = 0;
export let gameState = 'load';  // load, edit, play, zoomed

player = new Player();
tileMap = new TileMap();
camera = new Camera();
editor = new Editor(tileMap);
input = new InputManager(onMobileDevice);

export function init(mapStr) {
    const mapWidth = ((mapStr.indexOf('\n')+1) / 3) * Consts.TILE_SIZE;
    const mapHeight = (mapStr.length / (mapStr.indexOf('\n')+1) + 1) * Consts.TILE_SIZE;

    ctx = Utils.createHiPPICanvas('game-canvas', 816, 504, 1).getContext('2d');
    ctx.imageSmoothingEnabled = false;

    bgCtx = Utils.createHiPPICanvas('background-canvas', 816, 504, 1).getContext('2d');
    bgCtx.alpha = false;
    bgCtx.imageSmoothingEnabled = false;
    
    if(mapStr) {
        tileMap.loadMap(mapStr);
        camera.setWorldSize(mapWidth, mapHeight);
        gameState = 'play';
    }

    input.addListeners();
}


export function refreshMap() {
    editor.tileMap = tileMap.tiles;
    //console.log(editor.tileMap)
}

export function cleanup() { input.removeListeners(); }

const zoomedOutScrollRef = handleScroll.bind();
export function handleScroll(event) {
    camera.y += event.wheelDeltaY / -10;
}

function addScrollListener() {
    window.addEventListener("wheel", zoomedOutScrollRef, {passive: true});
    camera.y = 0;
}

function removeScrollListener() {
    window.removeEventListener("wheel", zoomedOutScrollRef, {passive: true});
}

export function toggleEditor() {
    if(gameState !== 'edit')  {
        addScrollListener();
        gameState = 'edit';
    }
    else {
        removeScrollListener();
        gameState = 'play';
    }
    editor.toggleEditor();
}

export function toggleZoom() {
    if(gameState === 'edit')    return;

    if(gameState !== 'zoomed')  {
        gameState = 'zoomed';
    }
    else  {
        gameState = 'play';
    }
}

export function resetPlayer() { 
    player.position.x = player.startPosition.x; 
    player.position.y = player.startPosition.y; 
}

export function mainLoop(time) {
    if(gameState === 'load') {
        if(tileMap.tiles) {
            editor.tileMap = tileMap.tiles;
            gameState = 'play';
        }
    }

    else if(gameState === 'edit') {
        if(!editor.tileMap) editor.tileMap = tileMap.tiles;
        tileMap.draw(ctx, bgCtx, camera, true);
        camera.update();
        editor.draw(ctx, camera);
    }

    else if(gameState === 'play' || gameState === 'zoomed') {        
        input.update(player);
        
        player.animate();
        player.update(tileMap.tiles);

        camera.update(player);

        tileMap.draw(ctx, bgCtx, camera, (gameState === 'zoomed') );
        player.draw(ctx);

        if(input.onMobileDevice) {
            input.drawMobileControls(ctx.canvas.getBoundingClientRect().height);
        }

        // animate sprites every 10 frames
        gameFrame++;
        if(gameFrame == 10) {
            gameFrame = 0;
            player.animFrame++;
        }
    }

    if(tileMap.needsUpdate) {
        editor.tileMap = tileMap.tiles;
        
        tileMap.needsUpdate = false;
    }
}
