// ref: https://stackoverflow.com/questions/15661339
export function createHiPPICanvas(elementId, w, h, scaleFactor) {
    let cv = document.getElementById(elementId) || document.createElement('canvas');
    let ratio = window.devicePixelRatio;
    cv.width = w * ratio;
    cv.height = h * ratio;
    cv.getContext("2d").scale(ratio*scaleFactor, ratio*scaleFactor);
    return cv;
} 