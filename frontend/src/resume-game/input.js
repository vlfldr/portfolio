import * as Consts from '@/resume-game/constants.js';
import * as Utils from '@/resume-game/utils.js';

let controlsSpritesheet = new Image();
controlsSpritesheet.src = 'game/img/gamepad.png';

let canWidth = Consts.SS_TILE_SIZE * 6;
let canHeight =  Consts.SS_TILE_SIZE + 2;   // button looks better with slight Y border

export default class InputManager {
    constructor(onMobileDevice) {
        // keyboard input
        this.pressedKeys = {
            left: false,
            up: false,
            down: false,
            right: false
        }

        // mobile input
        this.onMobileDevice = onMobileDevice;
        this.mobileTouches = [];

        if(onMobileDevice) {
            // create canvas
            this.controlsCanvas = Utils.createHiPPICanvas(null, canWidth, canHeight, 1);
            this.controlsCanvas.id = 'controls-canvas';
            document.body.append(this.controlsCanvas);

            this.controlsCtx = this.controlsCanvas.getContext('2d');
            this.controlsCtx.imageSmoothingEnabled = false;
            this.controlsCtx.globalAlpha = 0.5;

            this.recomputeSize();
        }
        // listeners sold seperately™️
    }

    // single bind to preserve callback for removeEventListener()
    handleKeyRef    = this.handleKey    .bind(this);
    touchStartRef   = this.touchStart   .bind(this);
    touchEndRef     = this.touchEnd     .bind(this);
    recomputeSizeRef= this.recomputeSize.bind(this);

    addListeners() {
        window.addEventListener("keydown",  this.handleKeyRef, false);
        window.addEventListener("keyup",    this.handleKeyRef, false);

        if(this.onMobileDevice) {
            this.controlsCanvas.addEventListener("touchstart",   this.touchStartRef, false);
            this.controlsCanvas.addEventListener("touchend",     this.touchEndRef, false);
            this.controlsCanvas.addEventListener("touchcancel",  this.touchEndRef, false);
            //this.controlsCanvas.addEventListener("touchmove",    , false);
            window.addEventListener('resize', this.recomputeSizeRef, false);
        }
    }

    removeListeners () {
        window.removeEventListener("keydown",  this.handleKeyRef, false);
        window.removeEventListener("keyup",    this.handleKeyRef, false);

        if(this.onMobileDevice) {
            this.controlsCanvas.removeEventListener("touchstart",   this.touchStartRef, false);
            this.controlsCanvas.removeEventListener("touchend",     this.touchEndRef, false);
            this.controlsCanvas.removeEventListener("touchcancel",  this.touchEndRef, false);
            //this.controlsCanvas.removeEventListener("touchmove",    , false);
            window.removeEventListener('resize', this.recomputeSizeRef, false);
        }
    }

    drawMobileControls(yPos) {
        this.controlsCanvas.style["top"] = yPos - this.yOffset + "px";
        this.controlsCtx.clearRect(0, 0, canWidth, canHeight);

        // left arrow
        this.controlsCtx.drawImage(controlsSpritesheet, 
            (Consts.SS_TILE_SIZE * 3) - ((this.pressedKeys.left === true) ? Consts.SS_TILE_SIZE : 0), 
            0, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE,
            0, -3, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE);
        // right arrow
        this.controlsCtx.drawImage(controlsSpritesheet, 
            0 + ((this.pressedKeys.right === true) ? Consts.SS_TILE_SIZE : 0), 
            0, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE,
            Consts.SS_TILE_SIZE, -3, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE);
        // jump button
        this.controlsCtx.drawImage(controlsSpritesheet, 
            (Consts.SS_TILE_SIZE * 4) + ((this.pressedKeys.up === true) ? Consts.SS_TILE_SIZE : 0), 
            0, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE,
            79, 2, Consts.SS_TILE_SIZE, Consts.SS_TILE_SIZE);
    }

    update(player) {
        player.wantsWallGrab = false;
        if (this.pressedKeys.left) {
            player.velocity.x -= player.moveSpeed;
            player.flipX = true;
            player.wantsWallGrab = true;
        }
        if (this.pressedKeys.right) {
            player.velocity.x += player.moveSpeed;
            player.flipX = false;
            player.wantsWallGrab = true;
        }

        // jump
        if( this.pressedKeys.up 
            && (player.velocity.y == Infinity 
                || player.framesSinceGrounded < Consts.COYOTE_TIME_FRAMES
                || (player.framesOnWall < 0 && player.framesOnWall > -Consts.COYOTE_TIME_FRAMES) ) 
        ) {
            // reset wall frame counter if jumping from solid ground
            if(player.velocity.y == Infinity)               player.framesOnWall = 0;

            player.velocity.y = Consts.JUMP_VEL;
            player.framesSinceGrounded = Consts.COYOTE_TIME_FRAMES;

            // wall jump
            if(player.onWall != 0 && player.framesOnWall > 10) {
                player.velocity.x = -2 * player.onWall;
                player.onWall = 0;
            }

            if(player.framesOnWall < 0) {
                player.velocity.x = -2 * player.prevOnWall;
                player.prevOnWall = 0;
                player.framesOnWall = 0;
            }
        }

        // "velocity clamp" jump termination      https://2dengine.com/?p=platformers
        if(!this.pressedKeys.up && player.velocity.y < Consts.JUMP_VEL_MIN_TERM)   
            player.velocity.y = Consts.JUMP_VEL_MIN_TERM;
    }

    handleKey(e) {
        let key = Consts.KEY_CODES[e.keyCode];
        if(!key) return;
    
        e.preventDefault();
        this.pressedKeys[key] = (e.type == "keydown");
    }

    touchEnd(e) {
        e.preventDefault();

        // send keyup based on original touch (t) position
        for(let i = 0; i < e.changedTouches.length; i++) {
            let endTch = e.changedTouches[i];
            this.mobileTouches.forEach(t => {
                if(t.identifier === endTch.identifier) {
                    this.simulateKeypress(t, false);
                    this.mobileTouches.splice(this.mobileTouches.indexOf(t), 1);
                }
            });
        }
    }
    
    touchStart(e) {
        e.preventDefault();
        
        for(let i = 0; i < e.targetTouches.length; i++) {
            this.mobileTouches.push(e.targetTouches[i])
            this.simulateKeypress(e.targetTouches[i], true);
        }
    }

    simulateKeypress(tch, keyDown) {
        // map screen pos to canvas pos
        let touchX = (tch.clientX - Consts.SIDEBAR_SIZE) / (window.innerWidth - Consts.SIDEBAR_SIZE);
        if(this.orientation == 'landscape')
            touchX = tch.clientX / window.innerWidth;
        touchX *= 100;

        // 'buttons' are actually canvas zones on X axis
        if(touchX < Consts.SS_TILE_SIZE)    
            this.pressedKeys['left'] = keyDown;
        else if(touchX > Consts.SS_TILE_SIZE && touchX < Consts.SS_TILE_SIZE * 2)    
            this.pressedKeys['right'] = keyDown;
        else if(touchX > 100 - Consts.SS_TILE_SIZE)    
            this.pressedKeys['up']  = keyDown;
    }

    // TODO: would be cleaner to swap CSS classes
    recomputeSize() {
        this.orientation = window.innerWidth > window.innerHeight ? "landscape" : "portrait";
        this.yOffset = 0;
        this.xOffset = 0;

        if(!document.getElementById('controls-canvas') || !document.getElementById('game-canvas')) {
            return
        }

        // center horizontally when rotated
        if(this.orientation == 'landscape') {
            this.yOffset = document.getElementById('controls-canvas').getBoundingClientRect().height + 10;
            this.xOffset = (window.innerWidth - document.getElementById('game-canvas').getBoundingClientRect().width) / 2;

            document.getElementById('game-canvas').style["left"] = this.xOffset + 'px';
            document.getElementById('background-canvas').style["left"] = this.xOffset + 'px';
        } else {
            document.getElementById('game-canvas').style["left"] = Consts.SIDEBAR_SIZE + 'px';
            document.getElementById('background-canvas').style["left"] = Consts.SIDEBAR_SIZE + 'px';
        }
    }
}

    