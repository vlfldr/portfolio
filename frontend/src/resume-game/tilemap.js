import * as Consts from '@/resume-game/constants.js';
import * as Utils from '@/resume-game/utils.js';
import Tile from '@/resume-game/tile.js';

let tileSheet = new Image();
let backgroundImg = new Image();
let fontSpritesheet = new Image();

tileSheet.src = 'game/img/tilesheet.png';
backgroundImg.src = 'game/img/bg.png';
fontSpritesheet.src = 'game/fonts/alagard.png';

let tsLoaded = false;
tileSheet.onload = () => { tsLoaded = true; }

export default class TileMap {
    constructor() {
        this.tiles = null;

        this.tilesX = 0;
        this.tilesY = 0;
        this.needsUpdate = false;

        this.cacheCtx = null;
        this.cacheLoaded = false;
    }

    getPixelWidth() { return this.tilesX * Consts.TILE_SIZE; }

    getPixelHeight() { return this.tilesY * Consts.TILE_SIZE; }

    // opens file upload dialog
    importMap() {
        let input = document.createElement('input');
        input.type = 'file';
        input.val = null;
        input.onchange = e => { 
            let reader = new FileReader();
            reader.readAsText(e.target.files[0],'UTF-8');
            reader.onload = readerEvent => { 
                this.tiles = this.tilemapFromString(readerEvent.target.result);
                this.needsUpdate = true;
                this.cacheLoaded = false;
            }
        }
        input.click();
    }

    // load string directly into tilemap
    loadMap(str) { 
        this.tiles = this.tilemapFromString(str);
        this.needsUpdate = true;
        this.cacheLoaded = false;
     }

    // export to localStorage
    exportToBrowser() {
        window.localStorage.setItem("tileMap", JSON.stringify(this.tiles));
        alert('Map saved to localStorage!');
    }

    // export to map.txt & download
    exportToFile() {
        let str = ''
        this.tiles.forEach(row => {
            row.forEach(tile => {
                if(tile.tileCode.length != 2 && tile.tileCode == parseInt(tile.tileCode, 10) && parseInt(tile.tileCode, 10) < 10)
                    str += '0' + tile.tileCode + ' ';
                else
                    str += tile.tileCode + ' ';
            });
            str += '\n';
        });

        let element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(str));
        element.setAttribute('download', 'map.txt');
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }

    // 2D array from string
    tilemapFromString(str) {
        let rowStrings = str.split('\n');
        let rows = [];

        for (let i = 0; i < rowStrings.length; i++) {
            let tmpRow = [];
            for (let j = 0; j < rowStrings[i].length; j+=3) {
                tmpRow.push(new Tile(rowStrings[i].substring(j, j+2)))
            }

            rows.push(tmpRow);
        }

        this.tilesX = rows[0].length;
        this.tilesY = rows.length;

        this.randomizeTileMap(rows)

        return rows;
    }

    randomizeTileMap(tm) {
        const maxTileWidth = 6; 

        const tileCodeSizes = {
            1: [ 26, 27, 28, 1 ],
            2: [ 29, 30, 31, 32, 33 ],
            3: [ 34, 35 ],
            6: [ 36 ],
            'corner': [ 24, 25 ]
        }
        const vTiles = Object.values(tileCodeSizes).flat();

        for (let i = 0; i < tm.length; i++) {
            const yMax = tm.length-2

            for (let j = 0; j < tm[i].length; j++) {
                if(tm[i][j].tileCode !== '01')   continue

                const xMax = tm[i].length-1
                let segClass = ''
                let segLen = 1
                let segLens = [ 1 ]

                // true if adjacent tile SOLID AND EXISTS for each cardinal direction
                const right =   j < xMax  && ( tm[i][j+1].tileCode == '0F'  ||  vTiles.includes( Number( tm[i][j+1].tileCode )) )
                const down =    i < yMax  && ( tm[i+1][j].tileCode == '0F'  ||  vTiles.includes( Number( tm[i+1][j].tileCode )) )
                const left =    j > 0     && ( tm[i][j-1].tileCode == '0F'  ||  vTiles.includes( Number( tm[i][j-1].tileCode )) )
                const up =      i > 0     && ( tm[i-1][j].tileCode == '0F'  ||  vTiles.includes( Number( tm[i-1][j].tileCode )) )
                
                // 0. extreme corners of map
                if(i == 0 && j == 0 && right && down) {
                    segClass = 'topLeftCorner'
                } else if(i == 0 && j == tm[i].length-1 && left && down) {
                    segClass = 'topRightCorner'
                } else if(i == tm.length-1 && j == 0 && up && right) {
                    segClass = 'bottomLeftCorner'
                } else if(i == tm.length-1 && j == tm[i].length-1 && up && left) {
                    segClass = 'bottomRightCorner'
                }

                // 1. corners of segments
                if( ( !up || i == 0 ) && ( (!left && j < xMax) || j == 0) && down && right) {
                    segClass = 'topLeftCorner'
                } else if ( (!up || i == 0) && ((j > 0 && !right) || j == xMax) && down && left) {
                    segClass = 'topRightCorner'
                } else if ( ((i > 0 && i < yMax && !down) || i == yMax) && (!left || j == 0) && j < xMax && up && right ) {
                    segClass = 'bottomLeftCorner'
                } else if ( (i == yMax || (i > 0 && !down)) && (j == xMax || (j > 0 && !right)) && up && left  ) {
                    segClass = 'bottomRightCorner'
                }

                // 1a. change corner tile codes here to avoid them being overwritten below
                if(segClass != '') {
                    // corner tile codes: [24, 25]
                    let cCode = tileCodeSizes['corner'][0] + Math.floor(Math.random())

                    tm[i][j].tileSprite = Consts.SPRITES[cCode]
                    tm[i][j].tileCode = cCode.toString()

                    if(segClass == 'topLeftCorner') {
                        tm[i][j].tileRotation = 0       // un-rotated corner sprite is top left
                    } else if(segClass == 'topRightCorner') {
                        tm[i][j].tileRotation = 1       // 90 deg clockwise
                    } else if(segClass == 'bottomRightCorner') {
                        tm[i][j].tileRotation = 2
                    } else if(segClass == 'bottomLeftCorner') {
                        tm[i][j].tileRotation = 3
                    }
                }

                // 2. horizontal/vertical segments
                if(segClass == '') {
                    if (!right && !down)   continue

                    if(right) {
                        segClass = 'horizontal'

                        // search horizontally until non '01' tile or end of row
                        while(j+segLen < xMax && segLen < maxTileWidth && tm[i][j+segLen+1].tileCode == '01') {
                            segLen++
                            segLens.push(segLen)
                        }
                    }
                    else if(down) {
                        segClass = 'vertical'

                        // search vertically until non '01' tile or end of row
                        while(i+segLen < yMax && segLen < maxTileWidth && (tm[i+segLen+1][j].tileCode == '01'   )) {
                            segLen++
                            segLens.push(segLen)
                        }
                    }
                }

                // 3. pick random tile size which fits in segment
                if(segLens.length > 3) {
                    if (segLens.length < 6 || Math.random() > 0.5)    segLens = [ 1, 2, 3 ]
                    else                        segLens = [ 1, 2, 3, 6 ]
                }

                const randLen = segLens[Math.floor(Math.random() * segLens.length)]

                const newCode = tileCodeSizes[randLen][Math.floor(Math.random() * tileCodeSizes[randLen].length)]

                // 4. modify tile + corresponding no-draws
                if(segClass == 'horizontal') {
                    tm[i][j].tileCode = newCode.toString()
                    tm[i][j].tileSprite = Consts.SPRITES[newCode]
                    for(let z = 1; z < Consts.SPRITES[newCode].w; z++ ) {
                        tm[i][j+z].tileCode = '0F'
                        tm[i][j+z].tileSprite = Consts.SPRITES[0]
                    }
                }
                else if(segClass == 'vertical') {
                    tm[i][j].tileCode = newCode.toString()
                    tm[i][j].tileSprite = Consts.SPRITES[newCode]
                    tm[i][j].tileRotation = 1;
                    if(Math.random() > .5)  tm[i][j].tileRotation = 3;        // TODO: fix big sprite 90 deg rotation

                    for(let z = 1; z < Consts.SPRITES[newCode].w; z++ ) {
                        tm[i+z][j].tileCode = '0F'
                        tm[i+z][j].tileSprite = Consts.SPRITES[0]
                    }
                }
            }
        }
    }
                                                 
    //           `7MM"""Yb.                                    
    //             MM    `Yb.                                  
    //             MM     `Mb `7Mb,od8 ,6"Yb.`7M'    ,A    `MF'
    //             MM      MM   MM' "'8)   MM  VA   ,VAA   ,V  
    //             MM     ,MP   MM     ,pm9MM   VA ,V  VA ,V   
    //             MM    ,dP'   MM    8M   MM    VVV    VVV    
    //           .JMMmmmdP'   .JMML.  `Moo9^Yo.   W      W     
                                                              
    // draw map data to offscreen canvases once upon load
    loadCacheCtx(){
        this.cacheCtx = Utils.createHiPPICanvas(null, 
            Consts.TILE_SIZE * this.tilesX, 
            Consts.TILE_SIZE * this.tilesY, 1).getContext('2d');
        this.cacheCtx.imageSmoothingEnabled = false;

        this.drawTilesToCanvas(this.cacheCtx);

        this.cacheLoaded = true;
    }
    
    drawTilesToCanvas(ctx) {
        this.tiles.forEach((row, i) => {
            row.forEach((tile, j) => {
                if(tile.tileLetter != '')   this.drawLetter(j, i, tile.tileLetter, ctx);
                else if(tile.tileCode != '00')                        this.drawSprite(j, i, tile.tileSprite, tile.tileRotation, ctx);

                // if(tile.tileDebug)          drawSprite(j, i, SPRITES[15]);
                // tile.tileDebug = false;
            });
        });
    }

    drawSprite(tileX, tileY, sprite, rotationIndex, ctx){
        if(sprite == Consts.SPRITES[0]) return;     // empty tile

        if(rotationIndex != 0) {
            const tarX = (tileX * Consts.TILE_SIZE);
            const tarY = (tileY * Consts.TILE_SIZE);

            ctx.setTransform(1, 0, 0, 1, tarX, tarY);

            ctx.rotate((90 * rotationIndex) * (Math.PI / 180));

            if (rotationIndex == 1)    ctx.translate((Consts.TILE_SIZE * sprite.w), 0);
            if (rotationIndex == 3)    ctx.translate(0, (Consts.TILE_SIZE * sprite.h));
            
            tileX = -sprite.w;
            tileY = -sprite.h;
        }

        ctx.drawImage(tileSheet, sprite.x * Consts.SS_TILE_SIZE, sprite.y * Consts.SS_TILE_SIZE, 
            Consts.SS_TILE_SIZE * sprite.w, Consts.SS_TILE_SIZE * sprite.h, tileX * Consts.TILE_SIZE, tileY * Consts.TILE_SIZE, 
            Consts.TILE_SIZE * sprite.w, Consts.TILE_SIZE * sprite.h);

        ctx.setTransform(1, 0, 0, 1, 0, 0);
    }

    drawLetter(tileX, tileY, letter, ctx){
        let charCode = letter.charCodeAt(0) - 33;   // temp hack
        
        ctx.drawImage(fontSpritesheet, 12 * charCode, 0, 12, 16, 
            (tileX * Consts.TILE_SIZE), tileY * Consts.TILE_SIZE, 12, 16);
    }

    draw(ctx, bgCtx, camera, editMode) {
        // render tiles to cacheCtx once when resources are ready
        if(!this.cacheLoaded && this.tiles && tsLoaded) {
            this.loadCacheCtx();
        }

        ctx.clearRect(0, 0, this.getPixelWidth(), this.getPixelHeight());

        if(editMode) {
            if(this.cacheLoaded)    this.cacheLoaded = false;

            // editor view - scrolls vertically with mousewheel
            ctx.setTransform(.5, 0, 0, .5, 0, -camera.y);

            bgCtx.drawImage(backgroundImg, 0, 0);

            // draw each tile each frame for live editing
            this.drawTilesToCanvas(ctx);

            document.getElementById('background-canvas').style['width'] = '100%';
        }
        else {
            // scale to viewport size & scroll
            ctx.setTransform(1, 0, 0, 1, -camera.x, -camera.y);

            bgCtx.drawImage(backgroundImg, -camera.x * .25, -camera.y * .25);

            // draw pre-rendered tilemap to main canvas
            if(this.cacheLoaded)    ctx.drawImage(this.cacheCtx.canvas, 0, 0);
        }   
    }
}